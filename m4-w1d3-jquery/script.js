$(document).ready(function () {
	
	//1. Find the children of the "recipes" section.
    var recipesChildren = $("#recipes").children();
    console.log(recipesChildren);
	
	//2. Find the "titles" of "beef" recipes. 
    var beefTitle = $(".beef .title")
    console.log(beefTitle);
	
	// 3. Find the children of the "blackened-catfish" recipe.
    var catfishChildren = $("#blackened-catfish").children();
    console.log(catfishChildren);
	
	// 4. Find the parent of the "winter-fruit-salad" recipe.
    var saladParent = $("#winter-fruit-salad").parent();
    console.log(saladParent);
	
	// 5. Find the parents of the "curried-tilapia" recipe.
    var tilapiaParent = $("#curried-tilapia").parent();
    console.log(tilapiaParent);
	
	// 6. Find the siblings of "tailgate-chili" "made-it".
    var chiliSiblings = $("#tailgate-chili .made-it").siblings();
    console.log(chiliSiblings);

	// 7. Find the siblings of "vegetarian" recipes.
    var veggieSiblings = $(".vegetarian").siblings();
    console.log(veggieSiblings);
	
	// 8. Filter even items from the table-of-contents list.
    var evenNumbers = $("#table-of-contents").filter(":even");
    console.log(evenNumbers);

	// 9. Filter "made-it" checkbox from "black-bean-burrito" children.
    var madeIt = $("#black-bean-burrito").children().filter(".made-it");
    console.log(madeIt);

	// 10. Get the text for all h3 elements.
    var h3 = $("h3");
    console.log(h3);
	
	// 11. Set the text for the "cabbage-pie" title to "Homemade Cabbage Pie -- Yum!".
	$("#cabbage-pie > h3").text("Homemade Cabbage Pie -- Yum!");
	
	// 12. Set the html for the "cabbage-pie" title to `"Homemade Cabbage Pie<br/>Yum!"`.
	$("#cabbage-pie > h3").html("Homemade Cabbage Pie<br/>Yum!");
	
	// 13. Now for good measure, and to seal in the difference between text() and html(), set the text for the "cabbage-pie" title to `"Homemade Cabbage Pie<br>Yum!"`.
	$("#cabbage-pie > h3").text("Homemade Cabbage Pie<br/>Yum!");
	
	// 14. Add "Delicious recipe description and instructions go here." to all recipe introductory paragraphs.
	$("#recipes").before("Delicious recipe description and instructions go here.")
	
	// 15. Get input type of all "remarks".
    var remarks = $(".remarks").attr("type");
    console.log(remarks);

	// 16. Set input type to "text" for all "remarks".
	$(".remarks").attr("type", "text");
	
	// 17. Set value for "remarks" to "Your remarks go here."
	$(".remarks").val("Your remarks go here.");

	// 18. Check each "made-it" checkbox.
	$(".made-it").attr("checked", "checked");

	// 19. Add "poultry" to "table-of-contents" li
	$("ul").append("<li>Poultry</li>")
	
	
	
	
	
	
	
	
	
	
	
	});