-- The following queries utilize the "dvdstore" database.

-- 1. All of the films that Nick Stallone has appeared in
--    Rows: 30
SELECT title FROM film WHERE film_id IN (
SELECT film_id FROM film_actor WHERE actor_id IN (
SELECT actor_id FROM actor WHERE first_name = 'NICK' AND last_name = 'STALLONE'));

SELECt title
FROM film f
INNER JOIN film_actor fa 
ON f.film_id = fa.film_id 
INNER JOIN actor 
ON fa.actor_id=actor.actor_id 
WHERE actor.first_name ='NICK' AND actor.last_name = 'STALLONE';




-- 2. All of the films that Rita Reynolds has appeared in
--    Rows: 20
SELECT title FROM film WHERE film_id IN (
SELECT film_id FROM film_actor WHERE actor_id IN (
SELECT actor_id FROM actor WHERE first_name = 'RITA' AND last_name = 'REYNOLDS'));

-- 3. All of the films that Judy Dean or River Dean have appeared in
--    Rows: 46
SELECT title FROM film WHERE film_id IN (
SELECT film_id FROM film_actor WHERE actor_id IN (
SELECT actor_id FROM actor WHERE first_name = 'JUDY' AND  last_name = 'DEAN') AND
SELECT actor_id FROM actor WHERE first_name = 'RIVER' AND  last_name = 'DEAN'));

-- 4. All of the the ‘Documentary’ films
--    Rows: 68
SELECt f.title, c.name 
FROM film f 
INNER JOIN film_category fc ON f.film_id=fc.film_id 
INNER JOIN category c ON c.category_id=fc.category_id 
WHERE c.name='Documentary' 
ORDER BY f.title;

-- 5. All of the ‘Comedy’ films
--    Rows: 58
SELECT f.title, c.name 
FROM film f 
INNER JOIN film_category fc on f.film_id=fc.film_id 
INNER JOIN category c on c.category_id=fc.category_id 
WHERE c.name='Comedy' 
ORDER BY f.title;

-- 6. All of the ‘Children’ films that are rated ‘G’
--    Rows: 10 
SELECT f.title, c.name, rating
FROM film f 
INNER JOIN film_category fc on f.film_id=fc.film_id 
INNER JOIN category c on c.category_id=fc.category_id
WHERE c.name='Children' AND rating = 'G'
ORDER BY f.title;

-- 7. All of the ‘Family’ films that are rated ‘G’ and are less than 2 hours in length
--    Rows: 3
SELECT f.title, c.name, rating, length
FROM film f 
INNER JOIN film_category fc on f.film_id=fc.film_id 
INNER JOIN category c on c.category_id=fc.category_id
WHERE c.name='Family' AND rating = 'G' AND length < 120
ORDER BY f.title;

-- 8. All of the films featuring actor Matthew Leigh that are rated ‘G’
--    Rows: 9
SELECt f.title, f.rating 
FROM film f
INNER JOIN film_actor fa ON f.film_id = fa.film_id 
INNER JOIN actor ON fa.actor_id=actor.actor_id 
WHERE actor.first_name ='MATTHEW' AND actor.last_name = 'LEIGH' AND f.rating = 'G'; 

-- 9. All of the ‘Sci-Fi’ films released in 2006
--    Rows: 61
SELECT f.title, f.release_year
FROM film f 
INNER JOIN film_category fc on f.film_id=fc.film_id 
INNER JOIN category c on c.category_id=fc.category_id
WHERE c.name='Sci-Fi' AND release_year = 2006
ORDER BY f.title;

-- 10. All of the ‘Action’ films starring Nick Stallone
--     Rows: 2
SELECT f.title, c.name
FROM film f
INNER JOIN film_actor fa ON f.film_id = fa.film_id 
INNER JOIN actor ON fa.actor_id=actor.actor_id 
INNER JOIN film_category fc on f.film_id=fc.film_id 
INNER JOIN category c on c.category_id=fc.category_id
WHERE actor.first_name ='NICK' AND actor.last_name = 'STALLONE' AND c.name='Action' ; 


-- 11. The address of all stores, including street address, city, district, and country
--     Rows: 2
SELECT s.store_id, a.address, c.city, a.district, co.country
FROM  store s 
INNER JOIN address a on s.address_id=a.address_id
INNER JOIN city c ON c.city_id = a.city_id
INNER JOIN country co ON c.country_id = co.country_id
ORDER BY s.store_id;


-- 12. A list of all stores by ID, the store’s street address, and the name of the store’s manager
--     Rows: 2
SELECT s.store_id, a.address, (sf.last_name||', '||sf.first_name) as full_name 
FROM store s 
INNER JOIN staff sf on s.manager_staff_id=sf.staff_id
INNER JOIN address a on s.address_id=a.address_id
ORDER BY s.store_id;


-- 13. The first and last name of the top ten customers ranked by number of rentals 
--     Hint: #1 should be “ELEANOR HUNT” with 46 rentals, #10 should have 39 rentals
SELECT cu.first_name, cu.last_name, COUNT(r.rental_id) AS rental
FROM customer cu
INNER JOIN rental r ON r.customer_id=cu.customer_id
GROUP BY cu.first_name, cu.last_name
ORDER BY rental DESC LIMIT 10;

-- 14. The first and last name of the top ten customers ranked by dollars spent 
--     Hint: #1 should be “KARL SEAL” with 221.55 spent, #10 should be “ANA BRADLEY” with 174.66 spent
SELECT cu.first_name, cu.last_name, SUM(p.amount) AS spent
FROM customer cu
INNER JOIN payment p ON cu.customer_id = p.customer_id
GROUP BY cu.first_name, cu.last_name
ORDER BY spent DESC LIMIT 10;

-- 15. The store ID, street address, total number of rentals, total amount of sales (i.e. payments), and average sale of each store 
--     Hint: Store 1 has 7928 total rentals and Store 2 has 8121 total rentals
SELECT s.store_id, a.address, COUNT(r.rental_id) AS rental--, SUM(p.amount) AS sales, AVG(p.amount/r.rental_id) as average
FROM store s
INNER JOIN address a on s.address_id=a.address_id
INNER JOIN customer cu on cu.store_id = s.store_id 
INNER JOIN payment p ON cu.customer_id = p.customer_id 
INNER JOIN rental r ON r.customer_id=cu.customer_id
GROUP BY  s.store_id, a.address;

-- 16. The top ten film titles by number of rentals 
--     Hint: #1 should be “BUCKET BROTHERHOOD” with 34 rentals and #10 should have 31 rentals
SELECT f.title, COUNT(r.rental_id) AS rental
FROM film f
INNER JOIN inventory i ON i.film_id = f.film_id
INNER JOIN rental r ON r.inventory_id = i.inventory_id
GROUP BY f.title
ORDER BY rental DESC LIMIT 10;

-- 17. The top five film categories by number of rentals 
--     Hint: #1 should be “Sports” with 1179 rentals and #5 should be “Family” with 1096 rentals
SELECT c.name, COUNT(r.rental_id) AS rental
FROM film f
INNER JOIN inventory i ON i.film_id = f.film_id
INNER JOIN rental r ON r.inventory_id = i.inventory_id
INNER JOIN film_category fc on f.film_id=fc.film_id 
INNER JOIN category c on c.category_id=fc.category_id
GROUP BY c.name
ORDER BY rental DESC LIMIT 5;

-- 18. The top five Action film titles by number of rentals 
--     Hint: #1 should have 30 rentals and #5 should have 28 rentals
SELECT f.title, COUNT(r.rental_id) AS rental
FROM film f
INNER JOIN inventory i ON i.film_id = f.film_id
INNER JOIN rental r ON r.inventory_id = i.inventory_id
INNER JOIN film_category fc on f.film_id=fc.film_id 
INNER JOIN category c on c.category_id=fc.category_id
WHERE c.name='Action'
GROUP BY f.title
ORDER BY rental DESC LIMIT 5;


-- 19. The top 10 actors ranked by number of rentals of films starring that actor 
--     Hint: #1 should be “GINA DEGENERES” with 753 rentals and #10 should be “SEAN GUINESS” with 599 rentals
SELECT a.first_name, a.last_name, COUNT(r.rental_id) AS rental
FROM film f
INNER JOIN inventory i ON i.film_id = f.film_id
INNER JOIN rental r ON r.inventory_id = i.inventory_id
INNER JOIN film_actor fa ON f.film_id = fa.film_id 
INNER JOIN actor a ON fa.actor_id= a.actor_id 
GROUP BY a.first_name, a.last_name
ORDER BY rental DESC LIMIT 10;


-- 20. The top 5 “Comedy” actors ranked by number of rentals of films in the “Comedy” category starring that actor 
--     Hint: #1 should have 87 rentals and #10 should have 72 rentals
SELECT a.first_name, a.last_name, COUNT(r.rental_id) AS rental
FROM film f
INNER JOIN inventory i ON i.film_id = f.film_id
INNER JOIN rental r ON r.inventory_id = i.inventory_id
INNER JOIN film_actor fa ON f.film_id = fa.film_id 
INNER JOIN actor a ON fa.actor_id= a.actor_id
INNER JOIN film_category fc on f.film_id=fc.film_id 
INNER JOIN category c on c.category_id=fc.category_id 
WHERE c.name='Comedy'
GROUP BY a.first_name, a.last_name
ORDER BY rental DESC LIMIT 5;
