package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DollarAmountTest {
private DollarAmount newDollarAmount;

	@Before
	public void setup() {
		newDollarAmount = new DollarAmount(3355);
	}
	
	
	@Test
	public void test_dollar_amount_format() {
		int dollar = newDollarAmount.getDollars();
		Assert.assertEquals(33, dollar);
	}
	@Test
	public void test_cents_amount_format() {
		int cents = newDollarAmount.getCents();
		Assert.assertEquals(55, cents);
	}
	@Test
	public void test_string_amount_format() {
		String toString = newDollarAmount.toString();
		Assert.assertEquals("$33.55", toString);
	}
	@Test
	public void test_is_greater_format() {
		DollarAmount isGreaterThan = new DollarAmount (2000);
		Boolean isGreaterThanTest = newDollarAmount.isGreaterThan(isGreaterThan);
		Assert.assertTrue("not True" ,isGreaterThanTest);
	}
	@Test
	public void test_is_greater_and_equal_format() {
		DollarAmount isGreaterThanEqual = new DollarAmount (2000);
		Boolean isGreaterThanEqualTest = newDollarAmount.isGreaterThanOrEqualTo(isGreaterThanEqual);
		Assert.assertTrue("not True" ,isGreaterThanEqualTest);
	}
	@Test
	public void test_is_less_than_format() {
		DollarAmount isLessThan = new DollarAmount (50000);
		Boolean isLessThanTest = newDollarAmount.isLessThan(isLessThan);
		Assert.assertTrue("not True" ,isLessThanTest);
	}
	@Test
	public void test_is_less_than_or_equal_format() {
		DollarAmount isLessThanEqual = new DollarAmount (50000);
		Boolean isLessThanEqualTest = newDollarAmount.isLessThanOrEqualTo(isLessThanEqual);
		Assert.assertTrue("not True" ,isLessThanEqualTest);
	}
	@Test
	public void test_minus() {
		DollarAmount minus = new DollarAmount (1000);
		newDollarAmount.minus(minus);
		int dollarMinus = newDollarAmount.getDollars() - minus.getDollars();
		Assert.assertEquals(23 , dollarMinus);
	}
	@Test
	public void test_plus() {
		DollarAmount plus = new DollarAmount (1000);
		newDollarAmount.plus(plus);
		int dollarPlus = newDollarAmount.getDollars() + plus.getDollars();
		Assert.assertEquals(43 , dollarPlus);
	}
	@Test
	public void test_dollar_amount_is_negative_format() {
		newDollarAmount.isNegative();
		DollarAmount negative = new DollarAmount (-1000);
		boolean negativeTest = negative.isNegative();
		Assert.assertTrue("Is negative", negativeTest);
	}
	@Test
	public void test_negative() {
		int hashCode = newDollarAmount.hashCode();
		Assert.assertEquals(3355, hashCode);
	}
	@Test
	public void test_hash_code() {
		int hashCode = newDollarAmount.hashCode();
		Assert.assertEquals(3355, hashCode);
	}
	@Test
	public void equals_test_object (){
		newDollarAmount.isNegative();
		DollarAmount negative = new DollarAmount (-1000);
		boolean negativeTest = negative.isNegative();
		Assert.assertTrue("Is negative", negativeTest);
	}
}