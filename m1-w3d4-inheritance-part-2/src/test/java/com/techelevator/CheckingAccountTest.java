package com.techelevator;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CheckingAccountTest {
	private CheckingAccount ourCheckingAccount;

	@Before
	public void setup() {
		ourCheckingAccount = new CheckingAccount("Bill Clinton", "123456ABC");
	}

	@Test
	public void initialization_values_test() {
	    String accountHolderName = ourCheckingAccount.getAccountHolderName();
	    String accountNumber = ourCheckingAccount.getAccountNumber();
	    Assert.assertEquals("Bill Clinton", accountHolderName);
	    Assert.assertEquals("123456ABC", accountNumber);        
	}
	@Test
	public void withdraw_test() {
		 DollarAmount balance = new DollarAmount(10000);
		    int oneHundred = balance.getDollars();
		    ourBankAccount.getBalance();
		    Assert.assertEquals(100, oneHundred);
		}

}
