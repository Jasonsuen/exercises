package com.techelevator;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;

public class BankAccountTest{
	private BankAccount ourBankAccount;
		

		@Before
		public void setup() {
		    ourBankAccount = new BankAccount("Bill Clinton", "123456ABC");
		}

		@Test
		public void initialization_values_test() {
		    String accountHolderName = ourBankAccount.getAccountHolderName();
		    String accountNumber = ourBankAccount.getAccountNumber();
		    Assert.assertEquals("Bill Clinton", accountHolderName);
		    Assert.assertEquals("123456ABC", accountNumber);        
		}

		@Test
		public void get_balance_test() {
		    DollarAmount balance = new DollarAmount(10000);
		    int oneHundred = balance.getDollars();
		    ourBankAccount.getBalance();
		    Assert.assertEquals(100, oneHundred);
		}

		@Test
		public void deposit_test() {
			 DollarAmount balance = new DollarAmount(10000);
			    ourBankAccount.getBalance();
			    int deposit = balance.getDollars();
			    balance = (new DollarAmount(10000));
			    Assert.assertEquals(100, deposit);
		}
	}