package com.techelevator;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BankCustomerTest {
	private BankCustomer steve;

	@Before
	public void setup() {
		steve = new BankCustomer("Steve Jobs","1 Apple Way, Cupertino, CA", "408-222-5555");
	}

	@Test
	public void initialization_values_test() {
	    String name = steve.getName();
	    String address = steve.getAddress();
	    String phoneNumber = steve.getPhoneNumber();
	    Assert.assertEquals("Steve Jobs", name);
	    Assert.assertEquals("1 Apple Way, Cupertino, CA", address);   
	    Assert.assertEquals("408-222-5555", phoneNumber);   
	}
	@Test
	public void test_customer_account() {
	}
}
