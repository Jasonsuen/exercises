package com.techelevator;

public class SavingsAccount extends BankAccount {
	
	public SavingsAccount(BankCustomer customer, String accountNumber) {
	    super(customer, accountNumber);
	}

	@Override
	public DollarAmount withdraw(DollarAmount amountToWithdraw) {
	    if (balance.isGreaterThanOrEqualTo(new DollarAmount(0)) && balance.isLessThanOrEqualTo(new DollarAmount(15000))) {
	        if (amountToWithdraw.isGreaterThan(balance.minus(new DollarAmount(200)))) {
	            amountToWithdraw.equals(new DollarAmount(0));
	            System.out.println("You cannot withdraw more than your balance.");
	        } else {
	            balance = balance.minus(amountToWithdraw);
	            balance = balance.minus(new DollarAmount(200));
	        }
	    } else if (balance.isGreaterThan(new DollarAmount(15000))) {
	        if (amountToWithdraw.isGreaterThan(balance)) {
	            amountToWithdraw.equals(new DollarAmount(0));
	            System.out.println("You cannot withdraw more than your balance.");
	        } else {
	            balance = balance.minus(amountToWithdraw);
	        }
	    }
	    return balance;
	}
}
