package com.techelevator;

public class BankAccount {

BankCustomer customer;
String accountNumber;
DollarAmount balance;

public BankAccount (BankCustomer customer, String accountNumber) {
	this.customer = customer;
	this.accountNumber = accountNumber;
	}

public String getAccountNumber() {
	return accountNumber;
	}

public DollarAmount getBalance() {
return balance;
	}

public DollarAmount deposit(DollarAmount amountToDeposit) {
balance = balance.plus(amountToDeposit);
	return balance;
	}


public DollarAmount withdraw(DollarAmount amountToWithdraw) {
balance = balance.minus(amountToWithdraw);
	return balance;
	}

public void transfer(BankAccount destinationAccount, DollarAmount transferAmount) {
	balance = balance.minus(transferAmount);
	destinationAccount.deposit(transferAmount);
	}
}