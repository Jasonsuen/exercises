package com.techelevator;

public class BankCustomer {
	String name;
	String address;
	String phoneNumber;
	boolean isVip;
	String accountNumber;
	private CheckingAccount newChecking;
	private SavingsAccount newSavings;
	
	public BankCustomer (String name, String address, String phoneNumber) {
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
		
		isVip = false;
		}
	
	
	public void createCheckingAccount(BankCustomer customer) {
		newChecking = new CheckingAccount(customer, accountNumber);
	}
	
	public void createSavingsAccount(BankCustomer customer) {
		newSavings = new SavingsAccount(customer, accountNumber);
	}
	
	public String getName() { 
		return name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public boolean getIsVip() {
		if ((newChecking.balance.plus(newSavings.balance)).isGreaterThanOrEqualTo(new DollarAmount(2500000))) {
			return true;
		} else {
			return false;
		}
	}
	
	public void setIsVIP(boolean isVip) {
		this.isVip = isVip;
	}
}
