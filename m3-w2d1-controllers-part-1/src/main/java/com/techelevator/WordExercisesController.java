package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WordExercisesController {

	@RequestMapping("/lastTwoInput")
	public String handleLastTwoInput(HttpServletRequest request) {
		return "lastTwoInput";
	}

	@RequestMapping("/lastTwoResult")
	public String handleLastTwoResult(HttpServletRequest request) {
		List<String> original = new ArrayList<String>();
		List<String> flipped = new ArrayList<String>();

		for (int i = 1; i <= 10; i++) {
			String word = request.getParameter("word" + i);
			if (word != null) {
				String modified = null;
				if (word.length() >= 2) {
					modified = word.substring(0, word.length() - 2) + word.charAt(word.length() - 1)
							+ word.charAt(word.length() - 2);
				} else if (word.length() == 1) {
					modified = word;
				}

				if (modified != null) {
					original.add(word);
					flipped.add(modified);
				}
			}
		}

		request.setAttribute("original", original);
		request.setAttribute("flipped", flipped);

		return "lastTwoResult";
	}

	@RequestMapping("/orderedNameInput")
	public String handleOrderedNameInput(HttpServletRequest request) {
		return "orderedNameInput";
	}

	@RequestMapping("/orderedNameResult")
	public String handleOrderedNameResult(HttpServletRequest request) {

		String first = request.getParameter("firstName");
		String mi = request.getParameter("middleInitial");
		String last = request.getParameter("lastName");
		String orderFormat = request.getParameter("nameOrder");

		switch (orderFormat) {
		case "firstMiddleLast":
			orderFormat = (first + " " + mi + " " + last);
			break;
		case "firstLast":
			orderFormat = (first + " " + last);
			break;
		case "lastFirstMiddle":
			orderFormat = (last + ", " + first + " " + mi);
			break;
		case "lastFirst":
			orderFormat = (last + ", " + first);
			break;
		default:
			System.out.println("Missing name part");
		}

		request.setAttribute("orderFormat", orderFormat);
		return "orderedNameResult";

	}

	@RequestMapping("/colorizedNameInput")
	public String handleColorizedInput(HttpServletRequest request) {
		return "colorizedNameInput";
	}

	@RequestMapping("/colorizedNameResult")
	public String handleColorizedResult(HttpServletRequest request) {
		return "colorizedNameResult";
	}

	@RequestMapping("/fizzBuzzRevisitedInput")
	public String handleFizzBuzzRevisitedInput(HttpServletRequest request) {
		return "fizzBuzzRevisitedInput";
	}

	@RequestMapping("/fizzBuzzRevisitedResult")
	public String handlefizzBuzzRevisitedResult(HttpServletRequest request) {

		int[] ints = strToInts(request.getParameterValues("nums"));

		String div1 = request.getParameter("div1");
		String div2 = request.getParameter("div2");

		int num1 = Integer.parseInt(div1);
		int num2 = Integer.parseInt(div2);

		String fizz = request.getParameter("fizz");
		String buzz = request.getParameter("buzz");

		String[] outStrings = new String[ints.length];

		int j = 0;
		for (int item : ints) {
			String plug = "";
			if ((item % num1 == 0) && (item % num2 == 0)) {
				plug = String.valueOf(item) + " " + fizz + " " + buzz;
			} else if (item % num1 == 0) {
				plug = String.valueOf(item) + " " + fizz;
			} else if (item % num2 == 0) {
				plug = String.valueOf(item) + " " + buzz;
			} else {
				plug = String.valueOf(item);
			}
			outStrings[j] = plug;
			j++;
		}
		System.out.println("end");
		request.setAttribute("fizzBuzz", outStrings);
		return "fizzBuzzRevisitedResult";
	}
private int[] strToInts(String[] strs) {

		int[] ints = new int[strs.length];
		for (int i = 0; i < strs.length; i++) {

			ints[i] = Integer.parseInt(strs[i]);
		}

		return ints;
	}
	@RequestMapping("/babyLottoInput")
	public String handleBabyLottoInput(HttpServletRequest request) {
		return "babyLottoInput";
}
	@RequestMapping("/babyLottoResult")
	public String handleBabyLottoREsult(HttpServletRequest request) {
		
		
		
		
		
		return "babyLottoResult";
}
	
	@RequestMapping("/squirrelCigarParty")
	public String displaySquirrelCigarParty(HttpServletRequest request) {
		return "squirrelCigarParty";
}
	@RequestMapping("/squirrelCigarPartyResult")
	public String handleSquirrelCigarParty(HttpServletRequest request) {
		return "squirrelCigarPartyResult";
}


}
