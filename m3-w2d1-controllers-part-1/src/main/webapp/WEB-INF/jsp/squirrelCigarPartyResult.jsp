<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Squirrel Party- Result</title>
	</head>
		<h1>Squirrel Party</h1>
		<c:choose> 
			<c:when test = "${param.checkbox != null}">
			<img src="img/happy-squirrel.png" />
			</c:when>
			<c:when test = "${param.squirrels >= 40 && param.squirrel <= 60}">
			<img src="img/happy-squirrel.png" />
			</c:when>	
			<c:otherwise>
			<img src="img/sad-squirrel.png" />
			</c:otherwise>
			
			
		</c:choose>
	<body>
	</body>
</html>