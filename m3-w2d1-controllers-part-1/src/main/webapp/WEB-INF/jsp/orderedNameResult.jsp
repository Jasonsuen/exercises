<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Ordered Name - Result</title>
	</head>
	<body>
		<p>First Name: ${param.firstName}</p>
		<p>Middle Initial: ${param.middleInitial}</p>
		<p>Last Name: ${param.lastName}</p>
		
		<p>Ordered Name: ${orderFormat}</p>
	</body>
</html>