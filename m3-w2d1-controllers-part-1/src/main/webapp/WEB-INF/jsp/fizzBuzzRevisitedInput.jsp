<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>FizzBuzz Revisited Input</title>
	</head>
	<body>
			<h1>FizzBuzz Revisited</h1>
	
		<c:url var="formsResult" value='/fizzBuzzRevisitedResult'/>
		<form action = "${formsResult}" method="GET">
			<label for="div1">Divisible By:</label>	<br>	
			<input type= "text" name="div1"/><br/>	
			
			<label for="div2">Divisible By:</label>	<br>	
			<input type= "text" name="div2"/><br/>	
			
			
			<label for="fizz">Alternative Fizz:</label>	<br>	
			<input type= "text" name="fizz"/><br/>
			
			<label for="buzz">Alternative Buzz:</label>	<br>	
			<input type= "text" name="buzz"/><br/>
			
			<br>	
					
			<label for="nums"> Number 1:</label>
			<input type= "text" name="nums"/><br/>
					
			<label for="nums"> Number 2:</label>
			<input type= "text" name="nums"/><br/>
					
			<label for="nums"> Number 3:</label>
			<input type= "text" name="nums"/><br/>
					
			<label for="nums"> Number 4:</label>
			<input type= "text" name="nums"/><br/>
			
			<label for="nums"> Number 5:</label>
			<input type= "text" name="nums"/><br/>
			
			<br>	
			<input type= "submit" value="Submit"/><br/>
			
		</form>
	</body>
</html>