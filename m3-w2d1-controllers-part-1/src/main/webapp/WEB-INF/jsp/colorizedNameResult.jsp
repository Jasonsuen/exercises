<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Colorized Name - Result</title>
	</head>
		<h1> Colorized Name</h1>
	<body>
		<p>First Name: ${param.firstName}</p>
		<p>Last Name: ${param.lastName}</p>
		<c:forEach var = "colorValue" items="${paramValues.nameColor}">
		<p style= color:${colorValue};> ${param.firstName} ${param.lastName} </p><br>
		</c:forEach>
	</body>
</html>