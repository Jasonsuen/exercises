<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE>
<html>
<head>
<title>Fizz Buzz Revisited Result</title>
</head>
<body>
<h1>Fizz Buzz Revisited</h1>

<div>
<h3>Divisible By: ${ param.div1 }</h3>
<h3>Divisible By: ${ param.div2 }</h3>
<h3>Divisible By Both: ${ param.div2 } ${ param.div1 }</h3>
</div><br>

<div>
<h3>Alternate Fizz: ${ param.fizz }</h3>
<h3>Alternate Buzz: ${ param.buzz }</h3>
</div><br>

<c:forEach items="${fizzBuzz}" var="item">
<p style="color:black">${item}</p>

</c:forEach>

</body>
</html>



<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<title>FizzBuzz Revisited - Result</title>
</head>
<h1>FizzBuzz Revisited</h1>

<body>
<div>
<h3>Divisible By: ${ param.div1 }</h3>
<h3>Divisible By: ${ param.div2 }</h3>
<h3>Divisible By Both: ${ param.div2 } ${ param.div1 }</h3>
</div>
	<br>

<div>
<h3>Alternate Fizz: ${ param.fizz }</h3>
<h3>Alternate Buzz: ${ param.buzz }</h3>
</div>
	<br>

<c:forEach items="${fizzBuzz }" var="item">
<p style="color:black">${item}</p>

</c:forEach>


</body>
</html> --%>