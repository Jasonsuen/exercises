<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Ordered Name Input</title>
	</head>
	<body>
			<h1>Enter Name</h1>
	
		<c:url var="formResult" value='/orderedNameResult'/>
		<form action = "${formResult}" method="Get">
			<label for="firstName"> First Name:</label>
			<input type= "text" name="firstName" value="firstName"/><br/>
			
			<label for="middleInitial"> Middle Initial:</label>
			<input type= "text" name="middleInitial" value="middleInitial"/><br/>
			
			<label for="lastName">Last Name:</label>
			<input type= "text" name="lastName" value="lastName"/><br/>
			
			<h1>Choose Order</h1>
					
			<input type= "radio" name="nameOrder" value="firstMiddleLast"/>First MI Last<br/>
			<input type= "radio" name="nameOrder" value="firstLast"/>First Last<br/>
			<input type= "radio" name="nameOrder" value="lastFirstMiddle"/>Last, First MI<br/>
			<input type= "radio" name="nameOrder" value="lastFirst"/>Last, First<br/>
			<input type= "submit" value="Submit"/><br/>
			
			
		</form>
	</body>
</html>