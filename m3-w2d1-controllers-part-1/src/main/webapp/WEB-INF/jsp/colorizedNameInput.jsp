<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Colorized Name Input</title>
	</head>
	<body>
			<h1>Enter Name</h1>
	
		<c:url var="formResult" value='/colorizedNameResult'/>
		<form action = "${formResult}" method="Get">
			<label for="firstName"> First Name:</label>
			<input type= "text" name="firstName"/><br/>
			
			
			<label for="lastName">Last Name:</label>
			<input type= "text" name="lastName"/><br/>
			
			<h1>Choose color</h1>
					
			<input type= "checkbox" name="nameColor" value="red"/>Red<br/>
			<input type= "checkbox" name="nameColor" value="blue"/>Blue<br/>
			<input type= "checkbox" name="nameColor" value="green"/>Green<br/>
			<input type= "submit" value="Submit"/><br/>
			
			
		</form>
	</body>
</html>