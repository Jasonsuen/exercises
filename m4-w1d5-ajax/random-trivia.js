function onSuccess(data) {
		console.log(data);
	    var user = data.results[0];
		$('#quizQuestions').empty();
	
	 for(var diva = 0; diva < 10; diva++) {
	 	$('#quizQuestions').append('<div class=' + diva + '>');

	 	    $('.' + diva).html("<h3>" + data.results[diva].question + "</h3><br/>");
	 	    $('.' + diva).append('<ol>');
	 	    $('.' + diva).append('<li class="correct"><h4>' + data.results[diva].correct_answer + '</h4></li>');

	 	    for(var j = 0; j < 3; j++) {
				if(data.results[diva].incorrect_answers[j] != undefined) {
	 	        $('.' + diva).append('<li class="incorrect"><h4>' + data.results[diva].incorrect_answers[j] + '</h4></li>');
	 	    	}
	 	    	
	 	    $('.' + diva).append('</ol>');
	 	    $('#quizQuestions').append('</div>');
	 	}
	}
}
function onFailure(xhr, status, error) {
	 	console.log(error);
	 	}	   
  function refresh() {
  		$.ajax({
  			url: "https://crossorigin.me/https://www.opentdb.com/api.php?amount=10",
  			type: "GET",
  		 	dataType: 'json'
  		}).done(onSuccess)
  		.fail(onFailure);
  		}
		
		
$(document).ready(function () {
		refresh();
			$("#refreshButton").on('click', refresh);
		}); 
