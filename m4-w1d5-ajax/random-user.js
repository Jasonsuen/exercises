function onSuccess(data) {
		console.log(data);
		
		var user = data.results[0];
		
		
		$("#user-photo").attr("src", user.picture.large);
		$("#name").text(user.name.first + " " + user.name.last);
		$("#fullName").text(user.name.title + ". " + user.name.first + " " + user.name.last);
		$("#dob").text(user.dob);
		$("#joined").text(user.registered);
		$("#phone").text(user.cell);
		$("#address").text(user.location.street + ", " +  user.location.city + ", " + user.location.state + ", " + user.location.zipcode);
		$("#email").text(user.email);
		$("#username").text(user.login.username);
		$("#password").text(user.login.password);
		
		var table = $("#otherUsers");
		for(var i=1; i < data.results.length; i++) {
			user = data.results[i];
			
			var tableRow = $("<tr>");
			
			var image = $("<img>").prop("src", user.picture.thumbnail);
			var imageCell = $("<td>").append(image);
			tableRow.append(imageCell);
			
			var nameCell = $("<td>").text(user.name.first + " " + user.name.last);
			tableRow.append(nameCell);
			
			var emailCell = $("<td>").text(user.email);
			tableRow.append(emailCell);
			
			var phoneCell = $("<td>").text(user.phone);
			tableRow.append(phoneCell);
			
			
			table.append(tableRow);
		}
	}
function onFailure(xhr, status, error) {
		console.log(error);
		}
	
function refresh() {
		$.ajax({
			url: "https://randomuser.me/api/?results=10",
			type: "GET",
		 	dataType: 'json'
		}).done(onSuccess)
		.fail(onFailure);
		}
		
$(document).ready(function () {
	refresh();
	$("#refreshButton").on('click', refresh);
}); 