package com.techelevator;

import java.util.HashMap;
import java.util.Map;

public class Exercises {

	/*
	 * Dictionary/Map Exercises
	 */
	
	/*
	 * Given the name of an animal, return the name of a group of that animal
	 * (e.g. "Elephant" -> "Herd", "Rhino" - "Crash").  
	 * 
	 * The animal name should be case insensitive so "elephant", "Elephant", and 
	 * "ELEPHANT" should all return "herd". 
	 * 
	 * If the name of the animal is not found, null, or empty, return "unknown". 
	 * 
	 * Rhino -> Crash
	 * Giraffe -> Tower
	 * Elephant -> Herd
	 * Lion -> Pride
	 * Crow -> Murder
	 * Pigeon -> Kit
	 * Flamingo -> Pat
	 * Deer -> Herd
	 * Dog -> Pack
	 * Crocodile -> Float
	 *
	 * animalGroupName("giraffe") → "Tower"
	 * animalGroupName("") -> "unknown"
	 * animalGroupName("walrus") -> "unknown"
	 * 
	 */
	public String animalGroupName(String animalName) {
		Map<String, String> animalGroups = new HashMap<String, String>();
		animalGroups.put("rhino", "Crash");
		animalGroups.put("giraffe", "Tower");
		animalGroups.put("elephant", "Herd");
		animalGroups.put("lion", "Pride");
		animalGroups.put("crow", "Murder");
		animalGroups.put("pigeon", "Kit");
		animalGroups.put("flamingo", "Pat");
		animalGroups.put("deer", "Herd");
		animalGroups.put("dog", "Pack");
		animalGroups.put("crocodie", "Float");
		if(!animalGroups.containsKey(animalName.toLowerCase())){
			return "unknown";
		} else {
			return animalGroups.get(animalName.toLowerCase());
		}
	}

	/*
	 * Given an String item number (a.k.a. SKU), return the discount percentage if the item is on sale.
	 * If the item is not on sale, return 0.00.
	 * 
	 * If the item number is empty or null, return 0.00.
	 * 
	 * "KITCHEN4001" -> 0.20
	 * "GARAGE1070" -> 0.15
	 * "LIVINGROOM"	-> 0.10
	 * "KITCHEN6073" -> 0.40
	 * "BEDROOM3434" -> 0.60
	 * "BATH0073" -> 0.15
	 * 
	 * The item number should be case insensitive so "kitchen4001", "Kitchen4001", and "KITCHEN4001"
	 * should all return 0.20.
	 *  
	 * isItOnSale("kitchen4001") → 0.20
	 * isItOnSale("") → 0.00
	 * isItOnSale("GARAGE1070") → 0.15
	 * isItOnSale("dungeon9999") → 0.00 
	 * 
	 */
	public Double isItOnSale(String itemNumber) {
		Map<String, Double> stringItemNumber = new HashMap<String, Double>();
		stringItemNumber.put("kitchen4001", 0.20);
		stringItemNumber.put("garage1070", 0.15);
		stringItemNumber.put("livingroom", 0.10);
		stringItemNumber.put("kitchen6073", 0.40);
		stringItemNumber.put("bedroom3434", 0.60);
		stringItemNumber.put("bath0073", 0.15);
		
		if(!stringItemNumber.containsKey(itemNumber.toLowerCase())){
			return 0.00;
		} else {
			return stringItemNumber.get(itemNumber.toLowerCase());
		}
	}
	
	/*
	 * Modify and return the given map as follows: if "Peter" has more than 0 money, transfer half of it to "Paul",
	 * but only if Paul has less than $10s.
	 * 
	 * Note, monetary amounts are specified in cents: penny=1, nickel=5, ... $1=100, ... $10=1000, ...
	 * 
	 * robPeterToPayPaul({"Peter": 2000, "Paul": 99}) → {"Peter": 1000, "Paul": 1099}
	 * robPeterToPayPaul({"Peter": 2000, "Paul": 30000}) → {"Peter": 2000, "Paul": 30000}
	 * 
	 */
	public Map<String, Integer> robPeterToPayPaul(Map<String, Integer> peterPaul) {
			int peterMoney = peterPaul.get("Peter");
			int paulMoney =  peterPaul.get("Paul");	
			
			if(peterMoney > 0 && paulMoney < 1000)
			{
				int halfOfPetersMoney = peterMoney/2;
				paulMoney += halfOfPetersMoney;
				peterMoney -= halfOfPetersMoney;
			}
			
			peterPaul.put("Peter", peterMoney);
			peterPaul.put("Paul", paulMoney );
			return peterPaul;
	}
	
	/*
	 * Modify and return the given map as follows: if "Peter" has $50 or more, AND "Paul" has $100 or more,
	 * then create a new "PeterPaulPartnership" worth a combined contribution of a quarter of each partner's
	 * current worth.
	 * 
	 * peterPaulPartnership({"Peter": 50000, "Paul": 100000}) → {"Peter": 37500, "Paul": 750000, "PeterPaulPartnership": 63500}
	 * peterPaulPartnership({"Peter": 3333, "Paul": 1234567890}) → {"Peter": 3333, "Paul": 1234567890}
	 * 
	 */
	public Map<String, Integer> peterPaulPartnership(Map<String, Integer> peterPaul) {
			int peterMoney = peterPaul.get("Peter");
			int paulMoney =  peterPaul.get("Paul");	
			int peterPaulPartnership = 0;
			if(peterMoney >= 5000 && paulMoney >= 10000){
				peterPaulPartnership = (peterMoney/4) + (paulMoney/4);
				peterMoney -= peterMoney/4;
				paulMoney -= paulMoney/4;		
			}
			peterPaul.put("Peter", peterMoney);
			peterPaul.put("Paul", paulMoney );
			peterPaul.put("PeterPaulPartnership",  peterPaulPartnership);
			return peterPaul;
	}
	
	/*
	 * Given an array of non-empty strings, return a Map<String, String> where for every different string in the array, 
	 * there is a key of its first character with the value of its last character.
	 *
	 * beginningAndEnding(["code", "bug"]) → {"b": "g", "c": "e"}
	 * beginningAndEnding(["man", "moon", "main"]) → {"m": "n"}
	 * beginningAndEnding(["muddy", "good", "moat", "good", "night"]) → {"g": "d", "m": "t", "n": "t"}
	 */
	public Map<String, String> beginningAndEnding(String[] words) {
		Map<String, String> beginEnd = new HashMap<String, String>();
		int i = 0;
		for(i = 0; i < words.length ; i++){
		String newString = words[i];
		String firstLetter = newString.substring(0, 1);
		String secondLetter = newString.substring(newString.length()-1);
		beginEnd.put(firstLetter, secondLetter);
	}
		return beginEnd;
	}
	/*
	 * Given an array of strings, return a Map<String, Integer> with a key for each different string, with the value the 
	 * number of times that string appears in the array.
	 * 
	 * ** A CLASSIC **
	 * 
	 * wordCount(["a", "b", "a", "c", "b"]) → {"b": 2, "c": 1, "a": 2}
	 * wordCount([]) → {}
	 * wordCount(["c", "b", "a"]) → {"b": 1, "c": 1, "a": 1}
	 * 
	 */
	public Map<String, Integer> wordCount(String[] words) {
		Map<String, Integer> countTheLetter = new HashMap<String, Integer>();
		  for (String i: words) {
		    if (!countTheLetter.containsKey(i)) {  
		    	countTheLetter.put(i, 1);
		    }
		    else {
		      int count = countTheLetter.get(i);
		      countTheLetter.put(i, count + 1);
		    }
		  }
		  return countTheLetter;
		}
	/*
	 * Given an array of int values, return a Map<Integer, Integer> with a key for each int, with the value the 
	 * number of times that int appears in the array.
	 * 
	 * ** The lesser known cousin of the the classic wordCount **
	 * 
	 * integerCount([1, 99, 63, 1, 55, 77, 63, 99, 63, 44]) → {1: 1, 44: 1, 55: 1, 63: 3, 77: 1, 99:2}
	 * integerCount([107, 33, 107, 33, 33, 33, 106, 107]) → {33: 4, 106: 1, 107: 3}
	 * integerCount([]) → {}
	 * 
	 */
	public Map<Integer, Integer> integerCount(int[] ints) {
		Map<Integer, Integer> intCount = new HashMap<Integer, Integer>();
		  for (Integer i: ints) {
		    if (!intCount.containsKey(i)) {  
		    	intCount.put(i, 1);
		    }
		    else {
		      int count = intCount.get(i);
		      intCount.put(i, count + 1);
		    }
		  }
		  return intCount;
		}
	
	/*
	 * Given an array of strings, return a Map<String, Boolean> where each different string is a key and value
	 * is true only if that string appears 2 or more times in the array.
	 * 
	 * wordMultiple(["a", "b", "a", "c", "b"]) → {"b": true, "c": false, "a": true}
	 * wordMultiple(["c", "b", "a"]) → {"b": false, "c": false, "a": false}
	 * wordMultiple(["c", "c", "c", "c"]) → {"c": true}
	 * 
	 */
	public Map<String, Boolean> wordMultiple(String[] words) {
		Map<String, Boolean> countBoolean = new HashMap<String, Boolean>();
		  for (String i: words) {
		    if (countBoolean.containsKey(i)) {  
		    	countBoolean.put(i, true);
		    } else {
		      countBoolean.put(i, false);
		    }
		  }
		  return countBoolean;
		}
	
	/*
	 * Given two maps, Map<String, Integer>, merge the two into a new map, Map<String, Integer> where keys in Map2, 
	 * and their Integer values, are added to the Integer values of matching keys in Map1. Return the new map.
	 * 
	 * Unmatched keys and their Integer values in Map2 are simply added to Map1.
	 *  
	 * consolidateInventory({"SKU1": 100, "SKU2": 53, "SKU3": 44} {"SKU2":11, "SKU4": 5})
	 * 	 → {"SKU1": 100, "SKU2": 64, "SKU3": 44, "SKU4": 5}
	 * 
	 */
	public Map<String, Integer> consolidateInventory(Map<String, Integer> mainWarehouse, Map<String, Integer> remoteWarehouse) {
		Map<String, Integer> newInventory = new HashMap<String, Integer>();
			newInventory.putAll(mainWarehouse);
		for(String key: remoteWarehouse.keySet()){
			if( newInventory.containsKey(key)){
				int mainValue = newInventory.get(key);
				int remoteValue = remoteWarehouse.get(key);
				int sum = mainValue + remoteValue;
				newInventory.replace(key, sum);
			} else {
				newInventory.put(key, remoteWarehouse.get(key));
			}
	}
		return newInventory;
	}

	/*
	 * Just when you thought it was safe to get back in the water --- last2Revisited!!!!
	 * 
	 * Given an array of strings, for each string, the count of the number of times that a substring length 2 appears 
	 * in the string and also as the last 2 chars of the string, so "hixxxhi" yields 1. 
	 * 
	 * We don't count the end substring, but the substring may overlap a prior position by one.  For instance, "xxxx"
	 * has a count of 2, one pair at position 0, and the second at position 1. The third pair at position 2 is the
	 * end substring, which we don't count.  
	 * 
	 * Return Map<String, Integer>, where the key is string from the array, and its last2 count.
	 *  
	 * last2Revisited(["hixxhi", "xaxxaxaxx", "axxxaaxx"]) → {"hixxhi": 1, "xaxxaxaxx": 1, "axxxaaxx": 2}
	 * 
	 */
	public Map<String, Integer> last2Revisted(String[] words) {
		Map<String, Integer> revisitTwo = new HashMap<String, Integer>();
		for(int i = 0; i < words.length ; i++){
		  String twoLast = words[i];
		  int count = 0;
		  
		  String last2 = twoLast.substring(twoLast.length() - 2);
		  for (int x=0; x < twoLast.length()-2; x++) {
		    if (last2.equals(twoLast.substring(x, x+2))) {
		      count++;
		    }
		  }
		  revisitTwo.put(words[i], count);
		}
	return revisitTwo;
	}
}
