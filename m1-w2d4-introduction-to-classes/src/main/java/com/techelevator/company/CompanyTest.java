package com.techelevator.company;


public class CompanyTest {

	public static void main(String[] args) {
		boolean allTestsPassed = true;
		// write code here that verifies the functionality of the Company class
		Company company = new Company();
		String name = "New Company";
		company.setName(name);
		if(company.getName() == null) {
			allTestsPassed = false;
			System.out.println("Test FAILED: No name found.");
		} else if (!name.equals(company.getName())){
			allTestsPassed = false;
			System.out.println("Test FAILED: Names did not match");
		}
		
		
		int numberOfEmployees = 20;
		if(company.getNumberOfEmployees() != 0) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Number of employees cannot start at 0");
		}
		
		company.setNumberOfEmployees(numberOfEmployees);
		if (numberOfEmployees != company.getNumberOfEmployees()){
			allTestsPassed = false;
			System.out.println("Test FAILED: Number of employees don't match");
		}
		String companySize = "small";
		if (!companySize.equals(company.getCompanySize())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Company size does not match");
		}
		
		numberOfEmployees = 70;
		company.setNumberOfEmployees(numberOfEmployees);
		if (numberOfEmployees != company.getNumberOfEmployees()){
			allTestsPassed = false;
			System.out.println("Test FAILED: Number of employees don't match");
		}
		
		companySize = "medium";
		if (!companySize.equals(company.getCompanySize())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Company size does not match");
		}
		
		
		numberOfEmployees = 260;
		
		company.setNumberOfEmployees(numberOfEmployees);
		if (numberOfEmployees != company.getNumberOfEmployees()){
			allTestsPassed = false;
			System.out.println("Test FAILED: Number of employees don't match");
		}
		companySize = "large";
		if (!companySize.equals(company.getCompanySize())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Company size does not match");
		}
	
		if(company.getRevenue() !=  0) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Revenue did not intialize correctly.");
		} 
		double revenue = 10000;
		company.setRevenue(revenue);
		if (revenue != (company.getRevenue())){
			allTestsPassed = false;
			System.out.println("Test FAILED: Revenue did not match");
		}
		
	
		if(company.getExpenses() !=  0) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Expenses did not intialize correctly.");
		}
		double expenses = 5000;
		company.setExpenses(expenses);
		if (revenue != (company.getRevenue())){
			allTestsPassed = false;
			System.out.println("Test FAILED: Expenses did not match");
		}
		
		double profit = revenue - expenses;
		if (profit != (company.getProfit())){
			allTestsPassed = false;
			System.out.println("Test FAILED: Profits did not match");
		}
		
	if(allTestsPassed){
	System.out.println("All Test Passed");
}
}
}