package com.techelevator.person;


public class PersonTest {

	public static void main(String[] args) {
		
		// write code here that verifies the functionality of the Person class
		boolean allTestsPassed = true;
		// write code here that verifies the functionality of the Company class
		Person person = new Person();
		String firstName = "Jason";
		person.setFirstName(firstName);
		if(person.getFirstName() == null) {
			allTestsPassed = false;
			System.out.println("Test FAILED: No first name found.");
		} else if (!firstName.equals(person.getFirstName())){
			allTestsPassed = false;
			System.out.println("Test FAILED: Names did not match");
		}
		String lastName = "Suen";
		person.setLastName(lastName);
		if(person.getLastName() == null) {
			allTestsPassed = false;
			System.out.println("Test FAILED: No last name found.");
		} else if (!lastName.equals(person.getLastName())){
			allTestsPassed = false;
			System.out.println("Test FAILED: Names did not match");
		}
		
		String fullName = firstName + " " + lastName;
		if (fullName == (person.getFullName())){
			allTestsPassed = false;
			System.out.println("Test FAILED: full names did not match");
		}
		int age = 32;
		person.setAge(age);
		if (age!= (person.getAge())){
			allTestsPassed = false;
			System.out.println("Test FAILED: Age did not match");
		}
		Boolean adult = true;
		if (!adult.equals(person.getAdult())) {
			allTestsPassed = false;
			System.out.println("Test FAILED: Company size does not match");
		}
	if(allTestsPassed){
		System.out.println("All Test Passed");
}
}
}
