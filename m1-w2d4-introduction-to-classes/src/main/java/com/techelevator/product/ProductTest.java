package com.techelevator.product;


public class ProductTest {

	public static void main(String[] args) {
		
		// write code here that verifies the functionality of your Product class
		boolean allTestsPassed = true;
		// write code here that verifies the functionality of the Company class
		Product product = new Product();
		String name = "Apples";
		product.setName(name);
		if(product.getName() == null) {
			allTestsPassed = false;
			System.out.println("Test FAILED: No name found.");
		} else if (!name.equals(product.getName())){
			allTestsPassed = false;
			System.out.println("Test FAILED: Names did not match");
		}
		Double price = 2.50;
		product.setPrice(price);
		if(product.getPrice() == 0) {
			allTestsPassed = false;
			System.out.println("Test FAILED: No price found.");
		} else if (price != (product.getPrice())){
			allTestsPassed = false;
			System.out.println("Test FAILED: Prices did not match");
		}
		int weightInOunces = 16;
		product.setWeightInOunces(weightInOunces);
		if(product.getWeightInOunces() == 0) {
			allTestsPassed = false;
			System.out.println("Test FAILED: No weight found.");
		} else if (weightInOunces != (product.getWeightInOunces())){
			allTestsPassed = false;
			System.out.println("Test FAILED: Weights did not match");
		}
		
	if(allTestsPassed){
	System.out.println("All Test Passed");
}
}
}