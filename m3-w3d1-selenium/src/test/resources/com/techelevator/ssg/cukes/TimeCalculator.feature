Feature: Travel Time Calculator

Scenario: Travel time test Mercury Walking
	Given I am on the Alien Travel Time page
	And I choose the travel planet Mercury
	And I choose the transport mode as Walking
	And I enter travel age 1
	When I request to calculate travel time
	Then the travel time is 2167.966
	Then the new age is 2168.966

Scenario: Travel time test Venus by Car
	Given I am on the Alien Travel Time page
	And I choose the travel planet Venus
	And I choose the transport mode as Car
	And I enter travel age 1
	When I request to calculate travel time
	Then the travel time is 29.366
	Then the new age is 30.366
	
Scenario: Travel time test Mars Boeing
	Given I am on the Alien Travel Time page
	And I choose the travel planet Mars
	And I choose the transport mode as Boeing 747 
	And I enter travel age 1
	When I request to calculate travel time
	Then the travel is 9.7489
	Then the new earth age is 10.7489
		
Scenario: Travel time test Jupiter Concorde
	Given I am on the Alien Travel Time page
	And I choose the travel planet Jupiter
	And I choose the transport mode as Concorde
	And I enter travel age 1
	When I request to calculate travel time
	Then the travel time is 33.0352
	Then the new age is 34.0352

Scenario: Travel time test Saturn BulletTrain
	Given I am on the Alien Travel Time page
	And I choose the travel planet Saturn
	And I choose the transport mode as Bullet Train
	And I enter travel age 1
	When I request to calculate travel time
	Then the travel is 452.1965
	Then the new earth age is 453.1965