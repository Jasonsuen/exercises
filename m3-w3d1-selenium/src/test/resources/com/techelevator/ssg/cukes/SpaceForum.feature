Feature: Space Forum 

Scenario: Check Space Forum Post
	Given I am on the Space Forum page
	And I enter username Jason
	And I enter subject test
	And I enter message this is just a test
	When I request to submit post
	Then the username is Jason
	Then the subject is test
	Then the message is this is just a test