Feature: Age Calculator

Scenario: Age test Mercury
	Given I am on the Alien Age Calculator page
	And I choose the planet Mercury
	And I enter age 1
	When I request to calculate age
	Then the age is 4.149377
	
Scenario: Age test Venus
	Given I am on the Alien Age Calculator page
	And I choose the planet Venus
	And I enter age 1
	When I request to calculate age
	Then the age is 1.6260

Scenario: Age test Earth
	Given I am on the Alien Age Calculator page
	And I choose the planet Earth
	And I enter age 1
	When I request to calculate age
	Then the age is 1
	
Scenario: Age test Mars
	Given I am on the Alien Age Calculator page
	And I choose the planet Mars
	And I enter age 1
	When I request to calculate age
	Then the age is 0.5319

Scenario: Age test Jupiter
	Given I am on the Alien Age Calculator page
	And I choose the planet Jupiter
	And I enter age 1
	When I request to calculate age
	Then the age is 0.0840

Scenario: Age test Saturn
	Given I am on the Alien Age Calculator page
	And I choose the planet Saturn
	And I enter age 1
	When I request to calculate age
	Then the age is 0.0339
	
Scenario: Age test Uranus
	Given I am on the Alien Age Calculator page
	And I choose the planet Uranus
	And I enter age 1
	When I request to calculate age
	Then the age is 0.0119

Scenario: Age test Neptune
	Given I am on the Alien Age Calculator page
	And I choose the planet Neptune
	And I enter age 1
	When I request to calculate age
	Then the age is 0.00606