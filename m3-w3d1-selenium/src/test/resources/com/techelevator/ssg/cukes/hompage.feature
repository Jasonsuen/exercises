Feature: Home Page

Scenario: Click on the age calculator
	Given I am on the home page
	And I click on the Alien Age link
	Then the title will Alien Age Calculator
	
	
Scenario: Click on the Weight calculator
	Given I am on the home page
	And I click on the Alien Weight link
	Then the title will Alien Weight Calculator
	
	
Scenario: Click on the Time calculator
	Given I am on the home page
	And I click on the Alien Travel Time link
	Then the title will Alien Travel Time
	
Scenario: Click on the Space Forum
	Given I am on the home page
	And I click on the Space Forum link
	Then the title will New Geek Post
	
Scenario: Click on the Space Store
	Given I am on the home page
	And I click on the Space Store link
	Then the title will Solar System Geek Gift Shop
	
Scenario: Click on the Space Store
	Given I am on the home page
	And I click on the Space Store link
	Then the title will Solar System Geek Gift Shop