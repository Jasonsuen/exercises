Feature: Weight Calculator

Scenario: Weight test Mercury
	Given I am on the Alien Weight Calculator page
	And I choose the planet Mercury
	And I enter weight 100
	When I request to calculate weight
	Then the weight is 37.00
	
Scenario: Weight test Venus
	Given I am on the Alien Weight Calculator page
	And I choose the planet Venus
	And I enter weight 100
	When I request to calculate weight
	Then the weight is 90.00
	
Scenario: Weight test Earth
	Given I am on the Alien Weight Calculator page
	And I choose the planet Earth
	And I enter weight 100
	When I request to calculate weight
	Then the weight is 100.00
		
Scenario: Weight test Mars
	Given I am on the Alien Weight Calculator page
	And I choose the planet Mars
	And I enter weight 100
	When I request to calculate weight
	Then the weight is 38.00
		
Scenario: Weight test Jupiter
	Given I am on the Alien Weight Calculator page
	And I choose the planet Jupiter
	And I enter weight 100
	When I request to calculate weight
	Then the weight is 265.00
	
Scenario: Weight test Saturn
	Given I am on the Alien Weight Calculator page
	And I choose the planet Saturn
	And I enter weight 100
	When I request to calculate weight
	Then the weight is 113.00

Scenario: Weight test Uranus
	Given I am on the Alien Weight Calculator page
	And I choose the planet Uranus
	And I enter weight 100
	When I request to calculate weight
	Then the weight is 109.00
	
Scenario: Weight test Neptune
	Given I am on the Alien Weight Calculator page
	And I choose the planet Neptune
	And I enter weight 100
	When I request to calculate weight
	Then the weight is 143.00	
	
	
	