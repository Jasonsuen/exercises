package com.techelevator.ssg.cukes;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techelevator.ssg.pageobject.AgeCalculator;
import com.techelevator.ssg.pageobject.AgeCalculatorResult;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@Component
public class AlienAgeStepTest {

	private WebDriver webDriver;
	private AgeCalculator ageCalculator;
	private AgeCalculatorResult ageCalculatorResult;

	@Autowired
	public AlienAgeStepTest(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.ageCalculator = new AgeCalculator(webDriver);
		this.ageCalculatorResult = new AgeCalculatorResult(webDriver);
	}

	@Given("^I am on the Alien Age Calculator page$")
	public void i_am_on_the_age_calulator_page() throws Throwable {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/alienAgeInput");
		ageCalculator = new AgeCalculator(webDriver);
	}

	@Given("^I choose the planet (.*)$")
	public void i_choose_a_planet_age(String planetName) throws Throwable {
		ageCalculator.choosePlanet(planetName);
	}
	
	
	@Given("^I enter age (.*)$")
	public void i_enter_age(String age) throws Throwable {
		ageCalculator.enterAge(age);
	}
	
	@When("^I request to calculate age$")
	public void i_submit() throws Throwable {
		ageCalculator.submitForm();
	
	}
	
	@Then("^the age is (.*)$")
	public void the_age_is(float ageResult) throws Throwable {
			Assert.assertEquals(ageResult, ageCalculatorResult.ageResult(), 0.005);
		}

}
