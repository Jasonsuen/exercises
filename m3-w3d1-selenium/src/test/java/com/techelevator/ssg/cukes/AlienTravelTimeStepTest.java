package com.techelevator.ssg.cukes;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techelevator.ssg.pageobject.TimeCalculator;
import com.techelevator.ssg.pageobject.TimeCalculatorResults;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@Component
public class AlienTravelTimeStepTest {
	private WebDriver webDriver;
	private TimeCalculator timeCalculator;
	private TimeCalculatorResults timeCalculatorResults;
	
	@Autowired
	public AlienTravelTimeStepTest(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.timeCalculator = new TimeCalculator(webDriver);
		this.timeCalculatorResults = new TimeCalculatorResults(webDriver);
	}
	@Given("^I am on the Alien Travel Time page$")
	public void i_am_on_the_alien_time_page() throws Throwable {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/alienTravelInput");
		timeCalculator = new TimeCalculator(webDriver);
	}

	@Given("^I choose the travel planet (.*)$")
	public void i_choose_a_planet_time(String planetName) throws Throwable {
		timeCalculator.choosePlanet(planetName);
	}
	
	
	@Given("^I choose the transport mode as (.*)$")
	public void i_enter_transportation_mode(String time) throws Throwable {
		timeCalculator.chooseTransportation(time);
	}
	@Given("^I enter travel age (.*)$")
	public void i_enter_age_time(String age) throws Throwable {
		timeCalculator.enterAge(age);
	}
	
	@When("^I request to calculate travel time$")
	public void i_submit_travel_time() throws Throwable {
		timeCalculator.submitForm();
	}
	
	@Then("^the travel time is (.*)$")
	public void the_age_is(float travelResult) throws Throwable {
			Assert.assertEquals(travelResult, timeCalculatorResults.travelResult(), 0.005);
		}
	
	@Then("^the new age is (.*)$")
	public void the_travel_age_is(float ageResult) throws Throwable {
			Assert.assertEquals(ageResult, timeCalculatorResults.travelAgeResult(), 0.005);

		}
	@Then("^the travel is (.+)$")
	public void the_age_is_two(float travelResultExtra) throws Throwable {
			Assert.assertEquals(travelResultExtra, timeCalculatorResults.travelResultExtra(), 0.005);
		}
	
	@Then("^the new earth age is (.+)$")
	public void the_travel_age_is_two(float ageResultExtra) throws Throwable {
			Assert.assertEquals(ageResultExtra, timeCalculatorResults.travelAgeResultExtra(), 0.005);

		}

	
}
