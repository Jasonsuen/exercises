package com.techelevator.ssg.cukes;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techelevator.ssg.pageobject.AgeCalculator;
import com.techelevator.ssg.pageobject.SpaceForum;
import com.techelevator.ssg.pageobject.SpaceForumResult;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@Component
public class SpaceForumStepTest {
	private WebDriver webDriver;
	private SpaceForum spaceForum;
	private SpaceForumResult spaceForumResult;

	@Autowired
	public SpaceForumStepTest(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.spaceForum = new SpaceForum(webDriver);
		this.spaceForumResult = new SpaceForumResult(webDriver);
	}
	@Given("^I am on the Space Forum page$")
	public void i_am_on_the_age_calulator_page() throws Throwable {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/spaceForumInput");
		spaceForum = new SpaceForum(webDriver);
	}

	@Given("^I enter username (.*)$")
	public void i_enter_username(String username) throws Throwable {
		spaceForum.enterUserName(username);
	}
	
	@Given("^I enter subject (.*)$")
	public void i_enter_subject(String subject) throws Throwable {
		spaceForum.enterSubject(subject);
	}
	
	@Given("^I enter message (.*)$")
	public void i_enter_message(String message) throws Throwable {
		spaceForum.enterMessage(message);
	}
	
	@When("^I request to submit post$")
	public void i_submit() throws Throwable {
		spaceForum.submitForm();
	
	}
	@Then("^the username is (.*)$")
	public void the_username_is(String forumResult) throws Throwable {
			Assert.assertEquals(forumResult, spaceForumResult.ForumUserName());
		}
	
	@Then("^the subject is (.*)$")
	public void the_subject_is(String forumResult) throws Throwable {
			Assert.assertEquals(forumResult, spaceForumResult.ForumSubject());
		}
	@Then("^the message is (.*)$")
	public void the_message_is(String forumResult) throws Throwable {
			Assert.assertEquals(forumResult, spaceForumResult.ForumMessage());
		}
	
	
}
