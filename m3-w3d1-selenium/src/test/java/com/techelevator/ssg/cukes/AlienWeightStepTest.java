package com.techelevator.ssg.cukes;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techelevator.ssg.pageobject.WeightCalculator;
import com.techelevator.ssg.pageobject.WeightCalculatorResult;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@Component
public class AlienWeightStepTest {
	private WebDriver webDriver;
	private WeightCalculator weightCalculator;
	private WeightCalculatorResult weightCalculatorResult;

	@Autowired
	public AlienWeightStepTest(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.weightCalculator = new WeightCalculator(webDriver);
		this.weightCalculatorResult = new WeightCalculatorResult(webDriver);

	}

	@Given("^I am on the Alien Weight Calculator page$")
	public void i_am_on_the_weight_calulator_page() throws Throwable {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/alienWeightInput");
		weightCalculator = new WeightCalculator(webDriver);
	}

	@Given("^I choose the planet in weight (.*)$")
	public void i_choose_a_planet_weight(String planetName) throws Throwable {
		weightCalculator.choosePlanet(planetName);
	}
	
	
	@Given("^I enter weight (.*)$")
	public void i_enter_weight(String weight) throws Throwable {
		weightCalculator.enterWeight(weight);
	}
	
	@When("^I request to calculate weight$")
	public void i_submit() throws Throwable {
		weightCalculator.submitForm();
	
	}
	
	@Then("^the weight is (.*)$")
	public void the_weight_is(float weightResult) throws Throwable {
			Assert.assertEquals(weightResult, weightCalculatorResult.weightResult(), 0.005);
		}
}
