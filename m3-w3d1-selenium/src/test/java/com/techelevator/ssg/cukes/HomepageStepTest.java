package com.techelevator.ssg.cukes;


import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techelevator.ssg.pageobject.HomePage;
import com.techelevator.ssg.pageobject.PageObject;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

	@Component
	public class HomepageStepTest {
		
		private WebDriver webDriver;
		private HomePage homePage;
		private PageObject page;
	
	@Autowired 
	public HomepageStepTest(WebDriver webDriver) {
		this.webDriver = webDriver;
		this.homePage = new HomePage(webDriver);
		
	}
	
	@Given ("^I am on the home page$")
	public void i_am_on_the_homepage() throws Throwable {
		webDriver.get("http://localhost:8080/m3-java-ssg-exercises-pair/");
		homePage = new HomePage(webDriver);
		
		page = null;
	}
	
	@Given("^I click on the (.+) link$")
	public void i_click_on_the_alien_age_link(String linkName) throws Throwable {
		page = homePage.clickLink(linkName);
		
	}
	@Then("^the title will (.+)$")
	public void the_page_title_is(String pageTitle) throws Throwable {
			Assert.assertEquals(pageTitle, page.getPageTitle());
		}
	}
	
	