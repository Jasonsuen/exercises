package com.techelevator.ssg.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

	private WebDriver webDriver;
		
	public HomePage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public List<WebElement> getPlanetElements(){
		return webDriver.findElements(By.className("planet"));
	}
	public List<WebElement> getPlanetNameElements(){
		return webDriver.findElements(By.cssSelector(".planet > h2"));
	}
	public AgeCalculator clickAgeCalculatorLink(){
		WebElement ageCalculatorLink = webDriver.findElement(By.linkText("Alien Age"));
		ageCalculatorLink.click();
		return new AgeCalculator(webDriver);
	}
	public WeightCalculator clickWeightCalculatorLink(){
		WebElement weightCalculatorLink = webDriver.findElement(By.linkText("Alien Weight"));
		weightCalculatorLink.click();
		return new WeightCalculator(webDriver);
	}
	public TimeCalculator clickTimeCalculatorLink(){
		WebElement timeCalculatorLink = webDriver.findElement(By.linkText("Alien Travel Time"));
		timeCalculatorLink.click();
		return new TimeCalculator(webDriver);
	}
	
	public SpaceForum clickSpaceForumLink(){
		WebElement spaceForumLink = webDriver.findElement(By.linkText("Space Forum"));
		spaceForumLink.click();
		return new SpaceForum(webDriver);
	}
	
	public SpaceStore clickSpaceStoreLink(){
		WebElement spaceStoreLink = webDriver.findElement(By.linkText("Space Store"));
		spaceStoreLink.click();
		return new SpaceStore(webDriver);
	}
	public PageObject clickLink(String linkText){
		WebElement pageLink = webDriver.findElement(By.linkText(linkText));
		pageLink.click();
		
		if(linkText.equals("Alien Age")) {
			return new AgeCalculator(webDriver);
		}else if(linkText.equals("Alien Weight")){
			return new WeightCalculator(webDriver);
		}else if(linkText.equals("Alien Travel Time")){
			return new TimeCalculator(webDriver);
		}else if(linkText.equals("Space Forum")){
			return new SpaceForum(webDriver);
		}else if(linkText.equals("Space Store")){
			return new SpaceStore(webDriver);
		}else{
			return null;
		}
	}
}
