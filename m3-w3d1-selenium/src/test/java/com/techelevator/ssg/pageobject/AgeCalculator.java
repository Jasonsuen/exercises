package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AgeCalculator implements PageObject {

	private WebDriver webDriver;
	
	public AgeCalculator(WebDriver webDriver) {
		this.webDriver = webDriver;
	
	}
	
	@Override
	public String getPageTitle(){
		WebElement title = webDriver.findElement(By.cssSelector(".centeredPanel > h1"));
		return title.getText();
	}
	public AgeCalculator choosePlanet(String planetName) {
		Select planetSelection = new Select(webDriver.findElement(By.name("alienPlanet")));
		planetSelection.selectByVisibleText(planetName);
		return this;
	}
	
	public AgeCalculator enterAge(String age){
		WebElement ageField = webDriver.findElement(By.name("earthAge"));
		ageField.sendKeys(age);
		return this;
	}
	
	public AgeCalculatorResult submitForm() {
		WebElement submitButton = webDriver.findElement(By.xpath("//*[@id='weightButton']"));
		submitButton.click();
		return new AgeCalculatorResult(webDriver);
	}
	
}
