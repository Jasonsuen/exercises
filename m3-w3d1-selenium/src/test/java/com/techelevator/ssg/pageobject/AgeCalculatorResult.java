package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AgeCalculatorResult {
	private WebDriver webDriver;
	
	public AgeCalculatorResult(WebDriver webDriver) {
		this.webDriver = webDriver;
	
	}
	public float ageResult()  {
		//If you are 23 years old on Earth, you would be 95.4356846473029 years old on Mercury.
		WebElement result=webDriver.findElement(By.cssSelector("#calculatorResult > h2"));
		String[] parts = result.getText().split(" ");
		float value = Float.parseFloat(parts[11]);
		return value;
	}
}
