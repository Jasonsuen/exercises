package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class SpaceForum implements PageObject{
private WebDriver webDriver;
	
	public SpaceForum(WebDriver webDriver) {
		this.webDriver = webDriver;
	
	}
	@Override
	public String getPageTitle(){
		WebElement title = webDriver.findElement(By.cssSelector(".centeredPanel > h1"));
		return title.getText();
	}
	public SpaceForum enterUserName (String username){
		WebElement userNameField = webDriver.findElement(By.name("userName"));
		userNameField.sendKeys(username);
		return this;
	}
	
	public SpaceForum enterSubject (String subject){
		WebElement weightField = webDriver.findElement(By.name("subject"));
		weightField.sendKeys(subject);
		return this;
	}
	
	
	public SpaceForum enterMessage (String message){
		WebElement weightField = webDriver.findElement(By.name("message"));
		weightField.sendKeys(message);
		return this;
	}
	
	public SpaceForumResult submitForm() {
		WebElement submitButton = webDriver.findElement(By.xpath("//*[@id='forumInput']/div[4]/input"));
		submitButton.click();
		return new SpaceForumResult(webDriver);
	}
}
