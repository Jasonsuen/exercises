package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SpaceStore implements PageObject {
private WebDriver webDriver;
	
	public SpaceStore(WebDriver webDriver) {
		this.webDriver = webDriver;
	
	}
	@Override
	public String getPageTitle(){
		WebElement title = webDriver.findElement(By.cssSelector(".centeredPanel > h1"));
		return title.getText();
	}
	
	
}
