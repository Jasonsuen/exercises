package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TimeCalculatorResults {

		private WebDriver webDriver;
	
	public TimeCalculatorResults(WebDriver webDriver) {
		this.webDriver = webDriver;
	
	}
	public float travelResult()  {
		WebElement result= webDriver.findElement(By.cssSelector("#calculatorResult > h2"));
		String[] parts = result.getText().split(" ");
		float value = Float.parseFloat(parts[11]);
		return value;
	}
	
	public float travelResultExtra()  {
		WebElement result= webDriver.findElement(By.cssSelector("#calculatorResult > h2"));
		String[] parts = result.getText().split(" ");
		float value = Float.parseFloat(parts[12]);
		return value;
	}
	
	public float travelAgeResult()  {
		WebElement result= webDriver.findElement(By.cssSelector("#calculatorResult > h2"));
		String[] parts = result.getText().split(" ");
		float value = Float.parseFloat(parts[16]);
		return value;
	}
	public float travelAgeResultExtra()  {
		WebElement result= webDriver.findElement(By.cssSelector("#calculatorResult > h2"));
		String[] parts = result.getText().split(" ");
		float value = Float.parseFloat(parts[17]);
		return value;
	}
	
	
}
