package com.techelevator.ssg.pageobject;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.Select;

public class TimeCalculator implements PageObject{
	
private WebDriver webDriver;
	
	public TimeCalculator(WebDriver webDriver) {
		this.webDriver = webDriver;
	
	}
	@Override
	public String getPageTitle(){
		WebElement title = webDriver.findElement(By.cssSelector(".centeredPanel > h1"));
		return title.getText();
	}
	
	public TimeCalculator choosePlanet(String planetName) {
		Select planetSelection = new Select(webDriver.findElement(By.name("alienPlanet")));
		planetSelection.selectByVisibleText(planetName);
		return this;
	}
	
	public TimeCalculator chooseTransportation(String transportName) {
		Select transportSelection = new Select(webDriver.findElement(By.id("transportMode")));
		transportSelection.selectByVisibleText(transportName);
		return this;
	}
	
	
	public TimeCalculator enterAge (String age){
		WebElement weightField = webDriver.findElement(By.name("earthAge"));
		weightField.sendKeys(age);
		return this;
	}
	
	public TimeCalculatorResults submitForm() {
		WebElement submitButton = webDriver.findElement(By.xpath("//*[@id='main-content']/form/div[3]/div[2]/input"));
		submitButton.click();
		return new TimeCalculatorResults(webDriver);
	}
}
