package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SpaceForumResult {
private WebDriver webDriver;
	
	public SpaceForumResult(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public String ForumSubject()  {
		WebElement result= webDriver.findElement(By.cssSelector(".post:last-child > h4"));
		String subject = result.getText();
		return subject;
	}
	
	public String ForumUserName()  {
		WebElement result= webDriver.findElement(By.cssSelector(".post:last-child > p"));
		String[] parts = result.getText().split(" ");
		String value = parts[1];
		return value;
	}
	
	public String ForumMessage()  {
		WebElement result= webDriver.findElement(By.cssSelector(".post:last-child > p.message"));
		String message = result.getText();
		return message;
	}
	
	
}
