package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class WeightCalculator implements PageObject {

private WebDriver webDriver;
	
	public WeightCalculator(WebDriver webDriver) {
		this.webDriver = webDriver;
	
	}
	
	@Override
	public String getPageTitle(){
		WebElement title = webDriver.findElement(By.cssSelector(".centeredPanel > h1"));
		return title.getText();
	}
	
	public WeightCalculator choosePlanet(String planetName) {
		Select planetSelection = new Select(webDriver.findElement(By.id("alienPlanet")));
		planetSelection.selectByVisibleText(planetName);
		return this;
	}
	public WeightCalculator enterWeight (String weight){
		WebElement weightField = webDriver.findElement(By.name("earthWeight"));
		weightField.sendKeys(weight);
		return this;
	}
	
	public WeightCalculatorResult submitForm() {
		WebElement submitButton = webDriver.findElement(By.xpath("//*[@id='main-content']/form/div[2]/div/input"));
		submitButton.click();
		return new WeightCalculatorResult(webDriver);
	}

}
