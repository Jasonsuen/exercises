package com.techelevator.ssg.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WeightCalculatorResult {
	private WebDriver webDriver;
	
	public WeightCalculatorResult(WebDriver webDriver) {
		this.webDriver = webDriver;
	
	}
	public float weightResult()  {
		WebElement result= webDriver.findElement(By.cssSelector("#calculatorResult > h2"));
		String[] parts = result.getText().split(" ");
		float value = Float.parseFloat(parts[10]);
		return value;
	}
}
