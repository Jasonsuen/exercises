package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
public class FizzWriter {


public static void main(String[] args) {
    PrintWriter fileOutput = null;
    try {
        fileOutput = new PrintWriter(new FileOutputStream("FizzBuzz.txt"));
        for(int i = 1; i < 301; i++) {
            
            int x =0;
            int y=0;
            int z=0;
            
            if (i < 100 && i > 10) {
                String yz = Integer.toString(i);
                 y = Integer.parseInt(yz.substring(0, 1));
                 z = Integer.parseInt(yz.substring(1, 2));
                 
            } else  if(i > 100) {
                String xyz = Integer.toString(i);
                 x = Integer.parseInt(xyz.substring(0,1));
                 y = Integer.parseInt(xyz.substring(1,2));
                 z = Integer.parseInt(xyz.substring(2, 3));
            }
            
            if (i % 3 == 0 && i % 5 ==0) {
                fileOutput.println("FizzBuzz");
            } else if (i % 3 == 0 || (x == 3 || y == 3 || z == 3)) {
                fileOutput.println("Fizz");
            } else if (i % 5 == 0 || (x == 5 || y == 5 || z == 5)) {
                fileOutput.println("Buzz");
            } else {
                fileOutput.println(Integer.toString(i));
            } 
            
        }
    } catch (FileNotFoundException e) {
        System.out.println("File not found");
    } finally {
        fileOutput.close();
    }
    

}
}