$(document).ready(function() {
// 1. Add a click event handler to the "deepSkyBox" that displays an alert message: "Hey! You clicked me!" to the user.
	$("#deepSkyBox").click(function (event) {
   		alert("Hey! You clicked me!");
	});

// 2. When any of the rainbow boxes are double clicked, show that they are selected by adding the "selected" class.
// 3. If any of the selected rainbow boxes are double clicked, unselect it by removing the "selected" class.
	$("#box-rainbow .box").dblclick(function(event){
		if($(this).hasClass("selected")){
			$(this).removeClass("selected");
		} else {
			$(this).addClass("selected");
		}
	});	
// 4. When the "Hide Selected Boxes" button is clicked, hide any selected boxes from the page.
	$("#hideSelectedBoxes").click(function(event) {
		$("#box-rainbow .selected").hide();	
	});	

// 5. When the "Show All Boxes" button is clicked, show all boxes that are currently hidden.
	$("#showAllBoxes").click(function(event) {
		$("#box-rainbow .selected").show();	
	});	
	
// 6. When the "Refresh List" button is clicked, update the list items in the "selectedBoxList" with the names of each selected box (e.g. if Box 1 and 7 are selected then add two `<li>` items, one for each box).
	$("#refreshSelectedBoxList").click(function(event){
		$("#selectedBoxList").empty();
		$(".selected > p").each(function() {
		$("#selectedBoxList").append("<li>" + $(this).text() + "</li>");
	});
	
});
// 7. Add a border using the css-class `.selected-field` to the textboxes on the page when they receive focus. Remove the border when the textbox loses focus.
 	$("input").val("text").focus(function (event) {
    	$(this).addClass("selected-field")
		});
	$("input").val("text").focusout(function (event) {
		$(this).removeClass("selected-field")
	});
// 8. In the Form Address section, when the user indicates that they have a different billing address by checking the "My billing address is different box", display the fields in the "billingAddress" box. Hide those fields again if the user unchecks it.
	$("#differentBillingAddress").click(function(event){
		if(this.checked){
			$("#billingAddress").show();	
		}else{
			$("#billingAddress").hide();
		}
	});
	
// 9. As text is typed into the repeating text box display it out to the paragraph tag (#txtFieldOutput) element below it.
	$("#txtField").keyup(function(){
		$("#txtFieldOutput").html($(this).val());
	});
	
// 10. Add an event that when the "Mark all as checked" checkbox is checked, all of the sub-checkboxes are also checked.  If the user unchecks it, all checkboxes below it are unchecked.

	$("#checkAll").click(function(){
	$('input:checkbox').not(this).prop('checked', this.checked);
	});
	
// 11. **Challenge** Make the swap buttons work. If the boxes in the 2-4 position are swapped, switch places with the box immediately preceding it (i.e. 3 is clicked therefore 3 and 2 swap places). If boxes in positions 5-7 are clicked swap places with the box immediately following it. For box 1 switches places with the box immediately following, and box 8 switch places with the box immediately preceding.
	$(".box > button").click(function(event)) {
		var boxes = $("#box-rainbow").children();
		var box = $(this).parent().detach();
		var index = $(boxes).index(box);
		if($(this).text() == "<--Swap") {
			$("#box-rainbow .box").eq(index - 1).before($(box));
		} else {
			$("#box-rainbow .box").eq(index).after($(box));
		}	
	});
});