package com.techelevator;

import org.junit.*;


public class KataRomanNumeralsTest {
	/*1 ---> I
	10 --> X
	7 ---> VII
	*/
	
	private KataRomanNumerals roman;
	
	@Before
	public void setup() {
	    roman = new KataRomanNumerals();
	}

	@Test
	public void given_1_return_I() {
	    String results = roman.intoRomanNumbers(1);
	    
	    Assert.assertEquals("I", results);

	}
	@Test
	public void given_10_return_x() {
	    String results = roman.intoRomanNumbers(10);
	    
	    Assert.assertEquals("X", results);

	}
	@Test
	public void given_7_return_VII() {
	    String results = roman.intoRomanNumbers(7);
	    
	    Assert.assertEquals("VII", results);

	}
}
