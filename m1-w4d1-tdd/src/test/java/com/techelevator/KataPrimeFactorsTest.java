package com.techelevator;

import org.junit.*;


public class KataPrimeFactorsTest {
/*2 -> returns [2]
3 -> returns [3]
4 -> returns [2, 2]
6 -> returns [2, 3]
7 -> returns [7]
8 -> returns [2, 2, 2]
9 -> returns [3, 3]
10 -> returns [2, 5]
*/
	private KataPrimeFactors factors;
	
	@Before
	public void setup() {
		factors = new KataPrimeFactors();
	}
	@Test
	public void given_2_retrun_2() {
		int[] results = factors.factorize(2);
		Assert.assertEquals(1, results.length);
		Assert.assertEquals(2, results[0]);
		
	}
	@Test
	public void given_3_retrun_3() {
		int[] results = factors.factorize(3);
		Assert.assertEquals(1, results.length);
		Assert.assertEquals(3, results[0]);
		
	}
	@Test
	public void given_4_retrun_2_2() {
		int[] results = factors.factorize(4);
		Assert.assertEquals(2, results.length);
		Assert.assertEquals(2, results[0]);
		Assert.assertEquals(2, results[1]);
	}
	@Test
	public void given_6_retrun_2_3() {
		int[] results = factors.factorize(6);
		Assert.assertEquals(2, results.length);
		Assert.assertEquals(2, results[0]);
		Assert.assertEquals(3, results[1]);
	}
	@Test
	public void given_7_retrun_7() {
		int[] results = factors.factorize(7);
		Assert.assertEquals(1, results.length);
		Assert.assertEquals(7, results[0]);
	}
	@Test
	public void given_8_retrun_2_2_2() {
		int[] results = factors.factorize(8);
		Assert.assertEquals(3, results.length);
		Assert.assertEquals(2, results[0]);
		Assert.assertEquals(2, results[1]);
		Assert.assertEquals(2, results[2]);
	}
	@Test
	public void given_9_retrun_3_3() {
		int[] results = factors.factorize(9);
		Assert.assertEquals(2, results.length);
		Assert.assertEquals(3, results[0]);
		Assert.assertEquals(3, results[1]);
	}
	@Test
	public void given_10_retrun_2_5() {
		int[] results = factors.factorize(10);
		Assert.assertEquals(2, results.length);
		Assert.assertEquals(2, results[0]);
		Assert.assertEquals(5, results[1]);
	}
	@Test
	public void given_negative_10_retrun_empty_array() {
		int[] results = factors.factorize(-10);
		Assert.assertEquals(0, results.length);
	}
}
