package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataFizzBuzzTest {
/*1 ->  returns "1"
2 -> returns "2"
3 -> returns "Fizz"
4 -> returns "4"
5 -> returns "Buzz" 
15 -> returns "FizzBuzz"
... etc up to 100*/
	
	
	private KataFizzBuzz KFB;

	@Before
	public void setup() {
	    KFB = new KataFizzBuzz();
	}

	@Test
	public void given_1_return_1() {
	    String results = KFB.fizzy(1);
	    
	    Assert.assertEquals("1", results);
	}

	@Test 
	public void given_2_return_2() {
	    String results = KFB.fizzy(2);
	    
	    Assert.assertEquals("2", results);
	}

	@Test
	public void given_3_return_Fizz() {
	    String results = KFB.fizzy(3);
	    
	    Assert.assertEquals("Fizz", results);
	}

	@Test
	public void given_4_return_4() {
	    String results = KFB.fizzy(4);
	    
	    Assert.assertEquals("4", results);
	}

	@Test
	public void given_5_return_buzz() {
	    String results = KFB.fizzy(5);
	    
	    Assert.assertEquals("Buzz", results);
	}

	@Test
	public void given_15_return_fizzbuzz( ){
	    String results = KFB.fizzy(15);
	    
	    Assert.assertEquals("FizzBuzz", results);
	}

	@Test
	public void given_60_return_fizzbuzz( ){
	    String results = KFB.fizzy(60);
	    
	    Assert.assertEquals("FizzBuzz", results);
	}
	}