package com.techelevator;

import java.util.ArrayList;
import java.util.List;

public class KataPrimeFactors {

	public int[] factorize(int value){
		if ( value < 0){
			return new int[]{};
		}else if (isPrime(value)) {
			return new int[] {value};
		} else {	
			List<Integer> results = new ArrayList<Integer>();
			
			for(int i = 2; i < value; i++){
				if(value % i == 0) {
					results.add(i);
					int otherFactor = value/i;
					int[] otherFactorResults = factorize(otherFactor);
					
					for(int j = 0; j<otherFactorResults.length; j++) {
						results.add(otherFactorResults[j]);
					}
					break;
			}
			}
			int[] arrayResults = new int[results.size()];
			for (int i = 0;  i < results.size(); i++){
				arrayResults[i] = results.get(i);
		}
			return arrayResults;
		}
	}
	
	private boolean isPrime(int value) {
		boolean isPrime = true;
		for(int i = 2; i < value; i++) {
			if(value % i == 0) {
				isPrime = false;
			}
		}
		return isPrime;
	}
}