package com.techelevator;


import java.util.LinkedHashMap;
import java.util.Map;

public class KataRomanNumerals {
	 public String intoRomanNumbers(int number){
		  if(number<=0){
		   return "not defined";
		  } Map<Integer, String> numbers = new LinkedHashMap<Integer, String>();
       
		numbers.put(1000, "M");
        numbers.put(900, "CM");
        numbers.put(500, "D");
        numbers.put(400, "CD");
        numbers.put(100, "C");
        numbers.put(90, "XC");
        numbers.put(50, "L");
        numbers.put(40, "XL");
        numbers.put(10, "X");
        numbers.put(9, "IX");
        numbers.put(7, "VII");
        numbers.put(5, "V");
        numbers.put(4, "IV");
        numbers.put(1, "I");
        
        String roman="";
        for (Map.Entry<Integer, String> entry : numbers.entrySet()) {
        	   int key = entry.getKey();
        	   int j = number/key;
        	   if(j != 0){
        	    for (int i = 0; i < j; i++) {
        	     roman += numbers.get(key);
        	    }
        	    number = number % key;
        	   }   
        	  } 
        	  return roman;
        	 }
        	}