CREATE TABLE department
(
	department_number serial NOT NULL,
	department_name varchar(64) NOT NULL,	
	CONSTRAINT pk_department PRIMARY KEY (department_number)
);

CREATE TABLE project
(
	project_number serial NOT NULL,
	project_name varchar(64) NOT NULL,
	project_start_date date NOT NULL,
	CONSTRAINT pk_project PRIMARY KEY (project_number)
);

CREATE TABLE employee
(
	employee_number serial NOT NULL,
	job_title varchar(64) NOT NULL,
	last_name varchar(64) NOT NULL,
 	first_name varchar(64) NOT NULL,
	gender varchar(8) NOT NULL,
	birthdate date,
	hiredate date NOT NULL,
	belongs_to_department varchar,  
	CONSTRAINT pk_employee PRIMARY KEY (employee_number)
);

CREATE TABLE employee_department
(
	employee_number integer NOT NULL,
	department_number integer NOT NULL,
	CONSTRAINT pk_employee_department PRIMARY KEY (employee_number, department_number)
);

CREATE TABLE employee_project
(
	employee_number integer NOT NULL,
	project_number integer NOT NULL,
	CONSTRAINT pk_employee_department PRIMARY KEY (employee_number, project_number)
);

-- ADD DATA

INSERT INTO department (depart_name) VALUES ('HR Department');  
INSERT INTO department (depart_name) VALUES ('IT Department');  
INSERT INTO department (depart_name) VALUES ('Admin Department');

INSERT INTO project (project_name, project_start_date) VALUES ('Project 1', 2016-10-10);  
INSERT INTO project (project_name, project_start_date) VALUES ('Project 2', 2016-10-02);  
INSERT INTO project (project_name, project_start_date) VALUES ('Project 3', 2016-10-09);  
INSERT INTO project (project_name, project_start_date) VALUES ('Project 4', 2016-09-09);

INSERT INTO employee (job_title, last_name, first_name, gender, birthdate, hiredate, belongs_to_department) VALUES ('CEO', 'SUEN','JASON', 'MALE', 1983-09-09, 2001-09-13, (SELECT department_name FROM department WHERE department_name = 'Admin Department'));  
INSERT INTO employee (job_title, last_name, first_name, gender, birthdate, hiredate, belongs_to_department) VALUES ('RECEPTIONIST','RITER', 'ELIZABETH',  'FEMALE', 1984-03-19, 2003-01-23, (SELECT department_name FROM department WHERE department_name = 'Admin Department'));
INSERT INTO employee (job_title, last_name, first_name, gender, birthdate, hiredate, belongs_to_department) VALUES ('HR MANAGER','GOMEZ', 'LIZZY', 'FEMALE', 1989-06-25, 2007-04-01, (SELECT department_name FROM department WHERE department_name = 'HR Department'));
INSERT INTO employee (job_title, last_name, first_name, gender, birthdate, hiredate, belongs_to_department) VALUES ('LEAD ENGINEER', 'CONKLIN','JASON', 'CONKLIN', 'MALE', 1970-07-09, 2001-12-04, (SELECT department_name FROM department WHERE department_name = 'IT Department'));
INSERT INTO employee (job_title, last_name, first_name, gender, birthdate, hiredate, belongs_to_department) VALUES ('HR REP', 'LEE', 'JOHNNY', 'MALE', 1976-09-09, 1999-05-11, (SELECT department_name FROM department WHERE department_name = 'HR Department'));
INSERT INTO employee (job_title, last_name, first_name, gender, birthdate, hiredate, belongs_to_department) VALUES ('ENGINEER', 'MARIA',  'FEMALE', 1985-06-10, 2004-02-14, (SELECT department_name FROM department WHERE department_name = 'IT Department'));
INSERT INTO employee (job_title, last_name, first_name, gender, birthdate, hiredate, belongs_to_department) VALUES ('ACCOUNT EXECUTIVE', 'SMITH', 'DEVON', 'MALE', 1957-02-16, 1989-10-10, (SELECT department_name FROM department WHERE department_name = 'Admin Department'));
INSERT INTO employee (job_title, last_name, first_name, gender, birthdate, hiredate, belongs_to_department) VALUES ('RECRUITER', 'JONES', 'ALLISON', 'FEMALE', 1995-07-12, 2002-11-26, (SELECT department_name FROM department WHERE department_name = 'HR Department'));


INSERT INTO employee_project(employee_number, project_number) VALUES ((SELECT employee_number FROM employee WHERE last_name = 'SUEN' AND first_name = 'JASON'), 'PROJECT 1');
INSERT INTO employee_project(employee_number, project_number) VALUES ((SELECT employee_number FROM employee WHERE last_name = 'GOMEZ' AND first_name = 'LIZZY'), 'PROJECT 2');
INSERT INTO employee_project(employee_number, project_number) VALUES ((SELECT employee_number FROM employee WHERE last_name = 'LEE' AND first_name = 'JOHNNY'), 'PROJECT 3');
INSERT INTO employee_project(employee_number, project_number) VALUES ((SELECT employee_number FROM employee WHERE last_name = 'JONES' AND first_name = 'ALLISON'), 'PROJECT 4');

-- ADD FOREIGN KEY


ALTER TABLE employee_department
ADD FOREIGN KEY(employee_number) REFERENCES employee(employee_number);

ALTER TABLE employee_department
ADD FOREIGN KEY(department_number) REFERENCES department(department_number);

ALTER TABLE employee_project
ADD FOREIGN KEY(employee_number) REFERENCES employee(employee_number);

ALTER TABLE employee_project
ADD FOREIGN KEY(project_number) REFERENCES project(project_number);