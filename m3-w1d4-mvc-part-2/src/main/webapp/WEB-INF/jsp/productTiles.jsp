<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="productHeader.jsp"%>

<h1>Toy Department</h1>
			<c:forEach var="product" items="${productList}">
					<div class='container2'>
						<a href ="productDetail?productId=${product.productId}"><img class="img-title2" src="img/${product.imageName}" /></a>
						<div class=''>
							<p class='bold'>${product.name} <c:if test="${product.isTopSeller() == true }"><p class ="red"> Best Seller!</p></c:if></p>
						</div>
							<p>by ${product.manufacturer}</p>
							<p class='red'>$ ${product.price}</p>
							<p>
								<span class='bold'>Weight </span>${product.weightInLbs} lbs
							</p>
							<img src="img/${Math.round(product.averageRating)}-star.png"
								class="star" />
					</div>
				</c:forEach>
				
<%@ include file="productFooter.jsp"%>