<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="productHeader.jsp"%>

<img class="img-title2" src="img/${product.imageName}" />
<div class='text'>
	<p class='bold'>${product.name}<c:if
			test="${product.isTopSeller() == true }">
			<span class="red">Best Seller!</span>
		</c:if>
	</p>
	<p>by ${product.manufacturer}</p>
	<img src="img/${Math.round(product.averageRating)}-star.png"
		class="star" />
	<p class='red'>$ ${product.price}</p>
	<p>
		<span class='bold'>Weight </span>${product.weightInLbs} lbs
	</p>
	<p class='detail'>
		<span class='bold'>Description</span> ${product.description}
	</p>
</div>
<%@ include file="productFooter.jsp"%>