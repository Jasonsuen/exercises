<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="productHeader.jsp"%>
		<h1>Toy Department</h1>
		<ul>
			<c:forEach var="product" items="${productList}">
				<div class='container'>
					<a href ="productDetail?productId=${product.productId}"><img class="img-title" src="img/${product.imageName}" /></a> 
					<div class='text'>
						<p class = 'bold'>${product.name}<c:if test="${product.isTopSeller() == true }"><span class ="red">Best Seller!
						</span></c:if></p>
						<p>by ${product.manufacturer}</p>
						<p class = 'red'>$ ${product.price}</p>
						<p><span class ='bold'>Weight </span>${product.weightInLbs} lbs</p>
						<img src="img/${Math.round(product.averageRating)}-star.png" class="star" />
					</div>
				</div>
			</c:forEach>

		</ul>
<%@ include file="productFooter.jsp"%>