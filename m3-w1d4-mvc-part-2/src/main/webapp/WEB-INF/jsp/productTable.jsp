<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ include file="productHeader.jsp"%>

		<h1>Toy Department</h1>
			<div class='container3'>
		<table>
		<tr>
			<td class="white"></td>
			<c:forEach var="product" items="${productList}">
				<td><a href ="productDetail?productId=${product.productId}"><img class="img-title" src="img/${product.imageName}" /></a> <c:if test="${product.isTopSeller() == true }"><span class ="red">Best Seller!
						</span></c:if></td>
			</c:forEach>
		</tr>
		
		<tr>
			<td class="white">Name</td>
			<c:forEach var="product" items="${productList}">
				<td>${product.name}</td>
			</c:forEach>
		</tr>
		
		<tr>
		<td class="white">Rating</td>
		<c:forEach var="product" items="${productList}">
			<td><img src="img/${Math.round(product.averageRating)}-star.png"
				class="star" /></td>
		</c:forEach>
		</tr>

		<tr>
			<td class="white">Mfr</td>
			<c:forEach var="product" items="${productList}">
				<td>${product.manufacturer }</td>
			</c:forEach>
		</tr>
		
		<tr>
			<td class="white">Price</td>
			<c:forEach var="product" items="${productList}">
				<td class="red">$ ${ product.price }</td>
			</c:forEach>
		</tr>
		<tr>
			<td class="white">wt.(in lbs)</td>
			<c:forEach var="product" items="${productList}">
				<td>${product.weightInLbs}</td>
			</c:forEach>
		</tr>
		<tr>

			</table>
			</div>
	<%@ include file="productFooter.jsp"%>