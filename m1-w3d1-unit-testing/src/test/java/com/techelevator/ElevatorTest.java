package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ElevatorTest {
	private Elevator ourElevator;
	
	@Before
	public void setup() {
		 ourElevator = new Elevator(2,12);
		 }
	
	@Test
	public void intialization_values_are_stored_correctly() {

		int shaftNumber = ourElevator.getShaftNumber(); //act
		int numberOfLevels = ourElevator.getNumberOfLevels();

		Assert.assertEquals(2, shaftNumber); //assert
		Assert.assertEquals(12, numberOfLevels);
	}
		
	@Test
	public void is_the_current_level_one() {
		
		int currentLevel = ourElevator.getCurrentLevel(); //act
	
		Assert.assertEquals(1, currentLevel); //assert	
		}
	
	@Test
	public void is_door_open(){
		boolean success = ourElevator.isDoorOpen();
		boolean isDoorOpen = ourElevator.isDoorOpen();
	
		Assert.assertTrue("The door is open", success);
		Assert.assertEquals(true, isDoorOpen);
}
	@Test
    public void door_open() {
    	ourElevator.closeDoor();
    	ourElevator.openDoor();
    	 
    	Assert.assertTrue("Opened door", ourElevator.isDoorOpen());
   
    }
	@Test
    public void door_close() {
    	ourElevator.closeDoor();
    	 
    	Assert.assertFalse("Closed door", ourElevator.isDoorOpen());
    }
	
	
	@Test
    public void go_up_and_go_down() {
		ourElevator.closeDoor();
    	boolean goUp = ourElevator.goUp(7);
    	boolean goDown = ourElevator.goDown(3);
    	Assert.assertEquals(true, goUp);
    	Assert.assertEquals(true, goDown);
	}
    @Test
        public void go_up_and_go_down_more() {	
    	
    	ourElevator.closeDoor();
    	ourElevator.openDoor();
    	boolean goUpAgain = ourElevator.goUp(11);
    	boolean goDownAgain = ourElevator.goDown(6);
    	Assert.assertEquals(false, goUpAgain);
    	Assert.assertEquals(false, goDownAgain);
    }
    @Test
        public void go_up_and_go_down_exceeding() {	
    	
    	ourElevator.closeDoor();
    	boolean goHigher = ourElevator.goUp(51);
    	boolean goLower = ourElevator.goDown(0);
    	Assert.assertEquals(false, goHigher);
    	Assert.assertEquals(false, goLower);
    }
	
}



	









