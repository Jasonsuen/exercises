package com.techelevator;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FruitTreeTest {
private FruitTree newFruitTree;
	
	@Before
	public void setup() {
		 newFruitTree = new FruitTree("Orange", 8);
	}
	
	@Test
	public void intialization_values_are_stored_correctly() {

		String typeOfFruit = newFruitTree.getTypeOfFruit();
		int piecesOfFruit = newFruitTree.getPiecesOfFruitLeft();
	
		
		Assert.assertEquals("Orange", typeOfFruit); //assert
		Assert.assertEquals(8, piecesOfFruit);
}
	@Test
	    public void pick_fruit() {
		 	boolean pickFruit = newFruitTree.PickFruit(10);
		 	
	    	Assert.assertTrue("Can't pick more than there are pieces",  pickFruit);	
	
}
}



