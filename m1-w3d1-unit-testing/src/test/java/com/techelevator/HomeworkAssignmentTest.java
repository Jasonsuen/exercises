package com.techelevator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class HomeworkAssignmentTest {
private HomeworkAssignment newHomeworkAssignment;

	@Before
	public void setup() {
	newHomeworkAssignment = new HomeworkAssignment(100);
	newHomeworkAssignment.setTotalScores(100);
	newHomeworkAssignment.setSubmitterName("John");
	}
	
	@Test
	public void intialization_values_are_stored_correctly() {
		
		int possibleScores = newHomeworkAssignment.getPossibleScores();	
		Assert.assertEquals(100, possibleScores); 
}
	@Test
	public void total_score() {
		int totalScores = newHomeworkAssignment.getTotalScores();	
		Assert.assertEquals(100, totalScores); 
}
	@Test
	public void submitter_name() {
		String submitterName = newHomeworkAssignment.getSubmitterName();	
		Assert.assertEquals("John", submitterName); 
}
	@Test
	public void set_total_marks_and_get_letter_grade(){

	    List<String> gradesList = new ArrayList<String>(Arrays.asList("F","D","C","B","A"));
	    int j = 0;
	    for(int i = 55; i < 100; i+=11){
	        
	        newHomeworkAssignment.setTotalScores(i);
	        
	        String grade = newHomeworkAssignment.getLetterGrade();
	        Assert.assertEquals(gradesList.get(j), grade);
	        j++;
	        
	    }
	        
	}
}



