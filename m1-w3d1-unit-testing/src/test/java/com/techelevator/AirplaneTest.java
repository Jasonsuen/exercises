package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AirplaneTest {
	private Airplane ourPlane;
	
	@Before
	public void setup() {
		 ourPlane = new Airplane(13,150, "plane1");
		 
	}
	
	@Test
	public void intialization_values_are_stored_correctly() {

		
		int firstClassSeats = ourPlane.getAvailableFirstClassSeats(); //act
		int coachSeats = ourPlane.getAvailableCoachSeats();
		String planeNumber = ourPlane.getPlaneNumber();
		
		Assert.assertEquals(13, firstClassSeats); //assert
		Assert.assertEquals(150, coachSeats) ;
		Assert.assertEquals("plane1", planeNumber);
		}
		
    @Test
    public void reserve_a_first_class_seat() {
    	boolean success = ourPlane.reserve(true, 1);
    	
    	int firstClassAvailable = ourPlane.getAvailableFirstClassSeats();
    	
    	Assert.assertTrue("We were not able to reserve a first class seat", success);
    	Assert.assertEquals(12, firstClassAvailable);
    }
    @Test
    public void reserve_a_coach_seat() {
        boolean success = ourPlane.reserve(false, 1);
        	
        int coachAvailable = ourPlane.getAvailableCoachSeats();
        	
        Assert.assertTrue("We were not able to reserve a coach seat", success);
        Assert.assertEquals(149, coachAvailable);
    }
    @Test
    public void reserve_too_many_first_class_seats(){
    	boolean success = ourPlane.reserve(true, 15);
        
    	int firstClassAvailable = ourPlane.getAvailableFirstClassSeats();
    	
    	 Assert.assertFalse("We were able to reserve more first class seats that the plane has", success);
    	 Assert.assertEquals(13, firstClassAvailable);
    }
    @Test
    public void reserve_too_many_coach_seats(){
    	boolean success = ourPlane.reserve(false, 151);
        
    	int coachAvailable = ourPlane.getAvailableCoachSeats();
    	
    	 Assert.assertFalse("We were able to reserve more coach class seats that the plane has", success);
    	 Assert.assertEquals(150, coachAvailable);
    }
    @Test
    public void book_a_first_class_seat() {
    	boolean success = ourPlane.reserve(true, 5);
    	
    	int bookedFirstClassSeats = ourPlane.getBookedFirstClassSeats();
    	 
    	Assert.assertTrue("We were not able to book a first class seat", success);
    	Assert.assertEquals(5, bookedFirstClassSeats);
    
    }
    @Test
    public void book_a_coach_seat() {
    	boolean success = ourPlane.reserve(false, 10);
    	
    	int bookedCoachSeats = ourPlane.getBookedCoachSeats();
    	 
    	Assert.assertTrue("We were not able to book a coach seat", success);
    	Assert.assertEquals(10, bookedCoachSeats);
    }
    	 @Test
    	    public void total_first_class_seats() {
    	    	boolean success = ourPlane.reserve(true, 13);
    	    	
    	    	int totalFirstClassSeats = ourPlane.getTotalFirstClassSeats();
    	    	 
    	    	Assert.assertTrue("We were not able to book a first class seat", success);
    	    	Assert.assertEquals(13, totalFirstClassSeats);
    	    
    }
    	    @Test
    	    public void total_coach_seats() {
    	    	boolean success = ourPlane.reserve(false, 150);
    	    	
    	    	int totalCoachSeats = ourPlane.getTotalCoachSeats();
    	    	 
    	    	Assert.assertTrue("We were not able to book a coach seat", success);
    	    	Assert.assertEquals(150, totalCoachSeats);	
    	
    }
    }











  






 


		



		




