package com.techelevator;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TelevisionTest {
private Television 	newTelevision;

	@Before
	public void setup() {
			newTelevision= new Television(4,true);
	 }
	@Test
	public void intialization_values_are_stored_correctly() {

		int currentVolume = newTelevision.getCurrentVolume(); 
		boolean isOn = newTelevision.isOn();
		
		Assert.assertEquals(4, currentVolume); 
		Assert.assertTrue("Is on", isOn);
	}
	@Test
	public void get_selected_channel() {
		
		int selectedChannel = newTelevision.getSelectedChannel(); 
	
		Assert.assertEquals(3, selectedChannel); //
	}
	
	@Test
    public void turn_on() {
    	newTelevision.turnOff();
    	newTelevision.turnOn();
    	boolean isOn = newTelevision.isOn();
    	 
    	Assert.assertEquals(true, isOn);
   
    }
	@Test
    public void turn_off() {
		newTelevision.turnOn();
		newTelevision.turnOff();
		
    	boolean isOff = newTelevision.isOn();
    	 
    	Assert.assertEquals(false, isOff);
    }

	@Test
   	public void raise_volume(){
		newTelevision.getCurrentVolume();
		newTelevision.raiseVolume();
   		Assert.assertTrue("Volume has increased", newTelevision.getCurrentVolume() <= 10);
     
	}
	
	@Test
   	public void decrease_volume(){
		newTelevision.getCurrentVolume();
		newTelevision.lowerVolume();
   		Assert.assertTrue("Volume has decreased", newTelevision.getCurrentVolume() >= 0);
     
	}
	@Test
   	public void change_channel(){
		newTelevision.turnOn();
		newTelevision.changeChannel(8);
   		Assert.assertEquals(8, newTelevision.getSelectedChannel());
     
	}
	@Test
   	public void change_channel_out_of_range(){
		newTelevision.turnOn();
		newTelevision.changeChannel(20);
   		Assert.assertFalse("Out of Range", newTelevision.getSelectedChannel() >= 18);
     
	}
	
	
}

