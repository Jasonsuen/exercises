package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SmartPhoneTest {
private SmartPhone newSmartPhone;

	@Before
		public void setup(){
		newSmartPhone = new SmartPhone ("614-296-0918", "Verizon", "iOS");	
	}

	@Test
	public void intialization_values_are_stored_correctly() {

		
		String phoneNumber = newSmartPhone.getPhoneNumber(); 
		String carrier = newSmartPhone.getCarrier();
		String operatingSystem = newSmartPhone.getOperatingSystem();
		
		Assert.assertEquals("614-296-0918",phoneNumber);
		Assert.assertEquals("Verizon",carrier);
		Assert.assertEquals("iOS",operatingSystem);
		
}
	
    @Test
    public void battery_charge() {
    	int batteryCharge = newSmartPhone.getBatteryCharge();
    	Assert.assertEquals(100,batteryCharge);
  
}
    @Test
	public void is_on_call(){
		boolean isOnCall = newSmartPhone.isOnCall();
		Assert.assertFalse("Is on call", isOnCall);

}
    @Test
	public void making_a_call(){
		boolean Call = newSmartPhone.Call("614-888-8888", 10);
		Assert.assertTrue("Failed to make a call", Call);
		Assert.assertTrue("Battery fully charged", (newSmartPhone.getBatteryCharge() < 100));
    }
    
    
    @Test
   	public void recharge_battery(){
   		newSmartPhone.Call("614-888-8888", 10);
   		Assert.assertTrue("Battery fully charged", newSmartPhone.getBatteryCharge() < 100);
   		newSmartPhone.RechargeBattery();
   		Assert.assertEquals(100,newSmartPhone.getBatteryCharge());
       }
    @Test
   	public void answer_phone(){
    	newSmartPhone.AnswerPhone();
    	boolean onCall = newSmartPhone.isOnCall();
    	Assert.assertEquals(true, onCall);
    	Assert.assertTrue("Can't answer Call",newSmartPhone.isOnCall());
    }
    @Test
   	public void hang_up_phone(){
    	newSmartPhone.HangUp();
   		boolean onCall = newSmartPhone.isOnCall();
   		Assert.assertEquals(true, !onCall);
   		Assert.assertTrue("Can't hang up phone", !onCall);

    }
    
}
	