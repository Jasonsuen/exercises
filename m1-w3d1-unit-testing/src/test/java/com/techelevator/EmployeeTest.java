package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmployeeTest {
private Employee newEmployee;
	
	@Before
	public void setup() {
		newEmployee = new Employee(11,"Bob","Barker", 1000000, "Pricing Department");
	}
	
	@Test
	public void intialization_values_are_stored_correctly() {

		
		int employeeId = newEmployee.getEmployeeId(); //act
		String firstName = newEmployee.getFirstName();
		String lastName = newEmployee.getLastName();
		double salary = newEmployee.getAnnualSalary();
		String fullName = newEmployee.getFullName();
		String department = newEmployee.getDepartment();
		
		Assert.assertEquals(11,employeeId); //assert
		Assert.assertEquals("Bob", firstName) ;
		Assert.assertEquals("Barker", lastName);
		Assert.assertEquals(1000000, salary, 0.01);
		Assert.assertEquals("Barker, Bob", fullName);
		Assert.assertEquals("Pricing Department", department);

}
	 @Test
	    public void salary_raise() {	
	 		double percentage = 1.0;
		 	double raiseAmount= newEmployee.getAnnualSalary() * percentage/100;
	    	double newSalary = 	newEmployee.getAnnualSalary() + raiseAmount;
	    	
	    	double actualRaiseAmount = (double) newEmployee.RaiseSalary(percentage);
	    	
	    	Assert.assertEquals(raiseAmount, actualRaiseAmount, 0.01);
			Assert.assertEquals(newSalary, newEmployee.getAnnualSalary(), 0.01);
	    	
	 }
}







