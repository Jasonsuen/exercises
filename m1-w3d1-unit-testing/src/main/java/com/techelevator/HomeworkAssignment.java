package com.techelevator;

public class HomeworkAssignment {
	
    private int totalScores;
    private int possibleScores;
    private String submitterName;

    
    
    /**
     * Homework assignment requires possible marks 
     * @param possibleMarks
     */
    public HomeworkAssignment(int possibleScores) {
        this.possibleScores = possibleScores;
    }

    /**
     * Total number of marks received 
     * @return
     */
    public int getTotalScores() {
        return totalScores;    
    }

    /**
     * Total number of marks received
     * @param totalScores
     */
    public int setTotalScores(int totalScores) {
    	this.totalScores = totalScores;
    	return totalScores;
    }

    /**
     * Possible number of marks to get right on the homework assignment. 
     * @return
     */
    public int getPossibleScores() {
        return possibleScores;            
    }
    /**
     * Name of the person who submitted the homework assignment 
     * @param submitterName
     */
    public void setSubmitterName(String submitterName) {
    	this.submitterName = submitterName;
    }


    /**
     * Name of the person who submitted the homework assignment 
     * @return
     */
    public String getSubmitterName() {
        return submitterName;
    }
  

    /**
     * Letter grade for the assignment. (90-100 A, 80-89 B, 70-79 C, 60-69 D, otherwise F) 
     * @return
     */
    public String getLetterGrade() {
        double percentage = (double)totalScores / possibleScores;

        if (percentage >= 0.9) {
            return "A";
        } 
        if (percentage >= 0.8) { 
            return "B";
        }
        if (percentage >= 0.7) {
            return "C";
        }
        if (percentage >= 0.6) {
            return "D";
        }                                                
        else 
        {
            return "F";
        }
        
    }
}

