package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SalariedWorkerTest{
private SalariedWorker  newSalariedWorker;

@Before
public void setup() {
	 newSalariedWorker = new SalariedWorker("Hillary","Clinton", 675000);
}

@Test
public void intialization_values_are_stored_correctly() {
	String firstName =newSalariedWorker.getFirstName();
	String lastName = newSalariedWorker.getLastName();
	int annualSalary = newSalariedWorker.getAnnualSalary();
	Assert.assertEquals("Hillary", firstName) ;
	Assert.assertEquals("Clinton", lastName);
	Assert.assertEquals(675000/52, annualSalary/52);
}

@Test
public void hours_worker_intilization() {
	int hoursWorked = newSalariedWorker.getHoursWorked();
	Assert.assertEquals(newSalariedWorker.getHoursWorked(),hoursWorked);
}
@Test
public void calculate_pay_intilization() {
	int calculateWeeklyPay = (int) newSalariedWorker.getCalculateWeeklyPay();
	Assert.assertEquals(newSalariedWorker.getCalculateWeeklyPay(),calculateWeeklyPay, 0.01);
}

}
