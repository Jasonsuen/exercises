package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VolunteerTest{
private Volunteer newVolunteer;

	@Before
	public void setup() {
		newVolunteer = new Volunteer("Mike","Jackson");
	}

	@Test
	public void intialization_values_are_stored_correctly() {
		String firstName =newVolunteer.getFirstName();
		String lastName = newVolunteer.getLastName();
		Assert.assertEquals("Mike", firstName) ;
		Assert.assertEquals("Jackson", lastName);
	}

	@Test
	public void hours_worker_intilization() {
		int hoursWorked = newVolunteer.getHoursWorked();
		Assert.assertEquals(newVolunteer.getHoursWorked(),hoursWorked);
	}
	@Test
	public void weekly_pay_intilization() {
		int calculateWeeklyPay = (int) newVolunteer.getCalculateWeeklyPay();
		Assert.assertEquals((int)newVolunteer.getCalculateWeeklyPay(),calculateWeeklyPay);
	}

}
