package com.techelevator;

public class SalariedWorker implements Worker {
	private String firstName;
	private String lastName;
	private int annualSalary;
	private int hoursWorked;
	
	public SalariedWorker ( String firstName, String lastName, int annualSalary) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.annualSalary = annualSalary;
		this.hoursWorked = (int) (Math.random() * 50 +1);
	}
	
	@Override
		public double getCalculateWeeklyPay() {
			int calculateWeeklyPay = annualSalary/52;
			return calculateWeeklyPay;
	}
			

	@Override
	public int getHoursWorked() {
		return hoursWorked;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}
	
	public int getAnnualSalary() {
		return annualSalary;
	}


	}
