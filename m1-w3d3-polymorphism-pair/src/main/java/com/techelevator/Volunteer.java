package com.techelevator;

public class Volunteer implements Worker {
	public double hourlyRate; 
	private String firstName;
	private String lastName;
	private int hoursWorked;
	
	public Volunteer ( String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.hoursWorked = (int) (Math.random() * 50 +1);
	}

	@Override
	public double getCalculateWeeklyPay() {
		double calculateWeeklyPay = hoursWorked * 0;
			return calculateWeeklyPay;
		}


	@Override
	public int getHoursWorked() {
		return hoursWorked;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}
}
