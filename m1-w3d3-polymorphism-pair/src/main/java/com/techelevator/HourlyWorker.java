package com.techelevator;


public class HourlyWorker implements Worker{
	
	public double hourlyRate; 
	private String firstName;
	private String lastName;
	private int hoursWorked;
	
	public HourlyWorker ( String firstName, String lastName, double hourlyRate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.hoursWorked = (int) (Math.random() * 50 +1);
		this.hourlyRate = hourlyRate;
	}
	
	@Override
	public int getHoursWorked() {
		return hoursWorked;
	}
	public void setHoursWorked(int hoursWorked) {
		this.hoursWorked = hoursWorked;
	}
	@Override
	public double getCalculateWeeklyPay() {
		if ( getHoursWorked() > 40) {
			double hourlyPay = hourlyRate * getHoursWorked();
			int overtime =  getHoursWorked() - 40;
			double calculateWeeklyPay= hourlyPay + (hourlyRate * overtime * 1.5); 
			return calculateWeeklyPay;
		} else {
			double calculateWeeklyPay = hourlyRate * getHoursWorked();
			return calculateWeeklyPay;
		}
	}


	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}
	public double getHourlyRate() {
		return hourlyRate;
	}

	}

