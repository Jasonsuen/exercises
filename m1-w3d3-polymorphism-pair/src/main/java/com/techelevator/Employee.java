package com.techelevator;

public class Employee {

	public String firstName;
	public String lastName;
	public int hoursWorked;
	public int pay;
	public double hourlyRate;
	public int annualSalary;
	
		public Employee (String firstName, String lastName, double hourlyRate, int annualSalary, int hoursWorked) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.hoursWorked = hoursWorked;
			this.hourlyRate = hourlyRate;
			this.annualSalary = annualSalary;
		}
		public static void main(String[] args) {
		Worker[] companyEmployees = new Worker[] { new SalariedWorker("Jason", "Suen", 100000), new HourlyWorker("Hilary", "Stork", 24.00), new Volunteer("Casey", "Borders") };
			System.out.println("Employee Hours" + "\t\t\t" +  "Worked" + "\t\t\t" + "Pay");
		int sum = 0;
		int paySum = 0;
		for(Worker worker  : companyEmployees) {
			String firstName = worker.getFirstName();
			String lastName = worker.getLastName();
			int hoursWorked = worker.getHoursWorked();
			double calculateWeeklyPay = worker.getCalculateWeeklyPay();
			System.out.println(lastName + ", " + firstName + "\t\t\t" + hoursWorked + "\t\t\t" + calculateWeeklyPay);
			sum +=  worker.getHoursWorked();
			paySum += worker.getCalculateWeeklyPay();
		} 
			System.out.println();
			System.out.println();
	
			System.out.println("Total Hours: " + sum);
			System.out.println("Total Pay: " + paySum);
		}
	}
