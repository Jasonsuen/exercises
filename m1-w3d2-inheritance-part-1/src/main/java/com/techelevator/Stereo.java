package com.techelevator;

public class Stereo extends Parts {

	private boolean hasAuxillaryInput;
	private int numberOfPresets;
	
	public Stereo(String partName, double partPrice, String partManufacturer, double partWeight,String partNumber){
		super(partName, partPrice, partManufacturer, partWeight, partNumber);
	}
	
	public boolean isHasAuxillaryInput() {
		return hasAuxillaryInput;
	}
	public void setHasAuxillaryInput(boolean hasAuxillaryInput) {
		this.hasAuxillaryInput = hasAuxillaryInput;
	}
	public int getNumberOfPresets() {
		return numberOfPresets;
	}
	public void setNumberOfPresets(int numberOfPresets) {
		this.numberOfPresets = numberOfPresets;
	}
	
}
