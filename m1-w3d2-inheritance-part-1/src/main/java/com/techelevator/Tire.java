package com.techelevator;

public class Tire extends Parts {

	
	private String speedRating;
	private int recommendedPSI;
	private boolean handlesSnowWell; 
	
	public Tire(String partName, double partPrice, String partManufacturer, double partWeight,String partNumber){
		super(partName, partPrice, partManufacturer, partWeight, partNumber);
	}
	
	public String getSpeedRating() {
		return speedRating;
	}
	public void setSpeedRating(String speedRating) {
		this.speedRating = speedRating;
	}
	public int getRecommendedPSI() {
		return recommendedPSI;
	}
	public void setRecommendedPSI(int recommendedPSI) {
		this.recommendedPSI = recommendedPSI;
	}
	public boolean isHandlesSnowWell() {
		return handlesSnowWell;
	}
	public void setHandlesSnowWell(boolean handlesSnowWell) {
		this.handlesSnowWell = handlesSnowWell;
	}
}
