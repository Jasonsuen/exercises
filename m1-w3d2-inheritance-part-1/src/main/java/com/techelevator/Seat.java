package com.techelevator;

public class Seat extends Parts {

	
	private boolean includesSeatHeater;
	private String materialType;
	
	public Seat(String partName, double partPrice, String partManufacturer, double partWeight,String partNumber){
		super(partName, partPrice, partManufacturer, partWeight, partNumber);
	}
	
	public boolean isIncludesSeatHeater() {
		return includesSeatHeater;
	}
	public void setIncludesSeatHeater(boolean includesSeatHeater) {
		this.includesSeatHeater = includesSeatHeater;
	}
	public String getMaterialType() {
		return materialType;
	}
	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}
	
	
}
