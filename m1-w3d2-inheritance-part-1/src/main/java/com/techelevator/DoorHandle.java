package com.techelevator;

public class DoorHandle extends Parts {

	private boolean hasTouchlessHandle;
	private String materialType;
	
	public DoorHandle(String partName, double partPrice, String partManufacturer, double partWeight,String partNumber){
		super(partName, partPrice, partManufacturer, partWeight, partNumber);
	}
	public boolean isHasTouchlessHandle() {
		return hasTouchlessHandle;
	}
	public void setHasTouchlessHandle(boolean hasTouchlessHandle) {
		this.hasTouchlessHandle = hasTouchlessHandle;
	}
	public String getMaterialType() {
		return materialType;
	}
	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}
	
	
	
}
