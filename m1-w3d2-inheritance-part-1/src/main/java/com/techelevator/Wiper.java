package com.techelevator;

public class Wiper extends Parts {

	private int length;
	private boolean isDriverSide;
	
	public Wiper(String partName, double partPrice, String partManufacturer, double partWeight,String partNumber){
		super(partName, partPrice, partManufacturer, partWeight, partNumber);
	}
	
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public boolean isDriverSide() {
		return isDriverSide;
	}
	public void setDriverSide(boolean isDriverSide) {
		this.isDriverSide = isDriverSide;
	}
	
	
}

