package com.techelevator;

public class Headlight extends Parts {

	
	private int lumens;
	private int lifeInHours;
	
	public Headlight(String partName, double partPrice, String partManufacturer, double partWeight,String partNumber){
		super(partName, partPrice, partManufacturer, partWeight, partNumber);
	}
	
	public int getLumens() {
		return lumens;
	}
	public void setLumens(int lumens) {
		this.lumens = lumens;
	}
	public int getLifeInHours() {
		return lifeInHours;
	}
	public void setLifeInHours(int lifeInHours) {
		this.lifeInHours = lifeInHours;
	}
}
