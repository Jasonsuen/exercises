package com.techelevator;

public class Parts {

	
	private String partName;
	private double partPrice;
	private String partManufacturer;
	private double partWeight;
	private String partNumber;
	
	public Parts(String partName, double partPrice, String partManufacturer, double partWeight,String partNumber){
		this.partName = partName;
		this.partPrice = partPrice;
		this.partManufacturer = partManufacturer;
		this.partWeight = partWeight;
		this.partNumber = partNumber;
		
	}
	
	public String getPartName() {
		return partName;
	}
	public void setPartName(String partName) {
		this.partName = partName;
	}
	public double getPartPrice() {
		return partPrice;
	}
	public void setPartPrice(double partPrice) {
		this.partPrice = partPrice;
	}
	public String getPartManufacturer() {
		return partManufacturer;
	}
	public void setPartManufacturer(String partManufacturer) {
		this.partManufacturer = partManufacturer;
	}
	public double getPartWeight() {
		return partWeight;
	}
	public void setPartWeight(double partWeight) {
		this.partWeight = partWeight;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	
}
