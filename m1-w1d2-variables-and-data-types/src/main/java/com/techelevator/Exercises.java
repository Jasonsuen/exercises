package com.techelevator;

public class Exercises {

	public static void main(String[] args) {
        
        /* 
        1. 4 birds are sitting on a branch. 1 flies away. How many birds are left on
        the branch? 
        */
		 int birdsOnBranch = 4; 
		 int birdFlewAway = 1;
		 int birdsLeftOnBranch = birdsOnBranch - birdFlewAway;
		 System.out.println("Birds left on the Branch: " + birdsLeftOnBranch);
        /* 
        2. There are 6 birds and 3 nests. How many more birds are there than
        nests? 
        */
		 int numberOfBirds = 6;
		 int numberOfNest = 3;
		 int birdsLeft = numberOfBirds -numberOfNest;
		 System.out.println("How many more birds than nests: " + birdsLeft);
        /* 
        3. 3 raccoons are playing in the woods. 2 go home to eat dinner. How
        many raccoons are left in the woods? 
        */
		 int raccoonsInWoods = 6;
		 int raccoonsHome = 3;
		 int raccoonsLeft = raccoonsInWoods - raccoonsHome;
		 System.out.println("Raccoons left in the woods: " + raccoonsLeft);
        /* 
        4. There are 5 flowers and 3 bees. How many less bees than flowers? 
        */
		 int numberOfFlowers = 5;
		 int numberOfBees = 3;
		 int lessBees= numberOfFlowers - numberOfBees;
		 System.out.println("How many less bees than flowers: " + lessBees);
        /* 
        5. 1 lonely pigeon was eating breadcrumbs. Another pigeon came to eat
        breadcrumbs, too. How many pigeons are eating breadcrumbs now? 
        */
		 int lonelyPigeon = 1;
		 int newPigeon = 1;
		 int breadcrumbPigeons= lonelyPigeon + newPigeon;
		 System.out.println("How many pigeons eating breadcrumbs: " + breadcrumbPigeons );
        /* 
        6. 3 owls were sitting on the fence. 2 more owls joined them. How many
        owls are on the fence now? 
        */
		 int orginalOwls = 3;
		 int newOwls = 2;
		 int owlsOnFence= orginalOwls + newOwls;
		 System.out.println("How many owls on the fence: " + owlsOnFence );
        /* 
        7. 2 beavers were working on their home. 1 went for a swim. How many
        beavers are still working on their home? 
        */
		 int swimmingBeaver = 1;
		 int workingBeaver= 2;
		 int stillWorkingBeaver= workingBeaver - swimmingBeaver;
		 System.out.println("Beavers still working on their home: " + stillWorkingBeaver);
        /* 
        8. 2 toucans are sitting on a tree limb. 1 more toucan joins them. How
        many toucans in all? 
        */
		 int orginalToucans = 2;
		 int newToucans = 1;
		 int toucansOnTree= orginalToucans + newToucans;
		 System.out.println("How many toucans on the tree limb: " + toucansOnTree);
        /* 
        9. There are 4 squirrels in a tree with 2 nuts. How many more squirrels
        are there than nuts? 
        */
		 int squirrelsTree = 4;
		 int treeNuts = 1;
		 int moreThanNuts= squirrelsTree + treeNuts;
		 System.out.println("How many more squirrels than there are nuts: " + moreThanNuts);
        /* 
        10. Mrs. Hilt found a quarter, 1 dime, and 2 nickels. How much money did
        she find? 
        */
		 int quarter = 25;
		 int dime = 10;
		 int nickel = 5;
				 int totalMoney = quarter + dime + nickel + nickel;
		System.out.println("How much money did she find: " + totalMoney +" cents");		 
        /* 
        11. Mrs. Hilt's favorite first grade classes are baking muffins. Mrs. Brier's
        class bakes 18 muffins, Mrs. MacAdams's class bakes 20 muffins, and
        Mrs. Flannery's class bakes 17 muffins. How many muffins does first
        grade bake in all? 
        */
		int brierMuffins = 18;
		 int macadamMuffins = 20;
		 int flanneryMuffins = 17;
				 int totalMuffins = brierMuffins + macadamMuffins + flanneryMuffins;
		System.out.println("Number of first grade muffins:  " + totalMuffins);
        /*
        12. Mrs. Hilt bought a yoyo for 24 cents and a whistle for 14 cents. How
        much did she spend in all for the two toys?
        */
        int yoyo = 24;
        int whistle = 14;
        int totalYoyoWhistle = yoyo + whistle;
        System.out.println("Total cost of toys: " + totalYoyoWhistle + " cents");
        /*
        13. Mrs. Hilt made 5 Rice Krispie Treats. She used 8 large marshmallows
        and 10 mini marshmallows.How many marshmallows did she use
        altogether?
        */
        int largeMarshmallows = 8;
        int smallMarshmallows = 10;
        int totalMarshmallows =  largeMarshmallows + smallMarshmallows;
        System.out.println("Total number of marshmallows: " + totalMarshmallows);
        /*
        14. At Mrs. Hilt's house, there was 29 inches of snow, and Brecknock
        Elementary School received 17 inches of snow. How much more snow
        did Mrs. Hilt's house have?
        */
        int hiltSnow = 29;
        int brecknockSnow = 17;
        int moreSnow = hiltSnow - brecknockSnow;
        System.out.println("How much more snow did Mrs, Hilt's house have: " + moreSnow);
       
        /*
        15. Mrs. Hilt has $10. She spends $3 on a toy truck and $2 on a pencil
        case. How much money does she have left?
        */
        int orginalMoney = 10;
        int costToyTruck = 3;
        int costPencil = 2;
        int amountLeft = orginalMoney - costToyTruck - costPencil;
        System.out.println("How much money was left: " + amountLeft + " dollars");

        /*
        16. Josh had 16 marbles in his collection. He lost 7 marbles. How many
        marbles does he have now?
        */
        int marbleCollection = 16;
        int marblesLost = 7;
        int marblesLeft = marbleCollection - marblesLost;
        System.out.println("How many marble are left: " + marblesLeft);
        /*
        17. Megan has 19 seashells. How many more seashells does she need to
        find to have 25 seashells in her collection?
        */
        int orginalSeashells = 19;
        int moreSeashells = 25;
        int neededSeashells = moreSeashells - orginalSeashells;
        System.out.println("How many more seashells does Megan need: " + neededSeashells);
        
        /*
        18. Brad has 17 balloons. 8 balloons are red and the rest are green. How
        many green balloons does Brad have?
        */
        int totalBallons = 17;
        int redBallons = 8;
        int blueBallons = totalBallons - redBallons;
        System.out.println("How many blue ballons did Brad have: " + blueBallons);
        /*
        19. There are 38 books on the shelf. Marta put 10 more books on the shelf.
        How many books are on the shelf now?
        */
        int booksOnShelf = 38;
        int newBooks = 10;
        int totalBooks = booksOnShelf + newBooks;
        System.out.println("How many books are on the shelf: " + totalBooks);

        /*
        20. A bee has 6 legs. How many legs do 8 bees have?
        */
        int beeLegs = 6;
        int totalBees = 8;
        int totalLegs = beeLegs * totalBees;
        System.out.println("How many legs do 8 bees have: " + totalLegs);
        		
        /*
        21. Mrs. Hilt bought an ice cream cone for 99 cents. How much would 2 ice
        cream cones cost?
        */
        double iceCreamCost = 0.99;
        double numberOfCones = 2;
        double totalIceCream = iceCreamCost * numberOfCones;
        System.out.println("How much would 2 ice cream cones cost: " + totalIceCream);
      
        /*
        22. Mrs. Hilt wants to make a border around her garden. She needs 125
        rocks to complete the border. She has 64 rocks. How many more rocks
        does she need to complete the border?
        */
        int rockTotal = 125;
        int rockHave = 64;
        int rockNeeded = rockTotal - rockHave;
        System.out.println("How many rocks does Mrs. Hilt need: " + rockNeeded);
        /*
        23. Mrs. Hilt had 38 marbles. She lost 15 of them. How many marbles does
        she have left?
        */
        int hiltMarbles = 38;
        int hiltLostMarbles = 15;
        int hiltMarblesLeft = hiltMarbles - hiltLostMarbles;
        System.out.println("How many marbles does Mrs. Hilt have: " + hiltMarblesLeft);
        /*
        24. Mrs. Hilt and her sister drove to a concert 78 miles away. They drove 32
        miles and then stopped for gas. How many miles did they have left to drive?
        */
        int totalMiles = 78;
        int gasMiles = 32;
        int milesLeft = totalMiles - gasMiles;
        System.out.println("How many miles are left: " + milesLeft);		
        /*
        25. Mrs. Hilt spent 1 hour and 30 minutes shoveling snow on Saturday
        morning and 45 minutes shoveling snow on Saturday afternoon. How
        much total time did she spend shoveling snow?
        */
        int snowTimeMorning = 90;
        int snowTimeAfternoon = 45;
        int snowTimeTotal = snowTimeMorning + snowTimeAfternoon;
        System.out.println("How much time did she spend shoveling: " + snowTimeTotal +" minutes");
        /*    
        26. Mrs. Hilt bought 6 hot dogs. Each hot dog cost 50 cents. How much
        money did she pay for all of the hot dogs?
        */
        double hotDogCost = 0.50;
        double numberOfHotDogs = 6;
        double totalCostOfHotDogs = hotDogCost * numberOfHotDogs;
        System.out.println("How much did she pay for hot dogs: " + totalCostOfHotDogs + " dollars");
       
        /*
        27. Mrs. Hilt has 50 cents. A pencil costs 7 cents. How many pencils can
        she buy with the money she has?
        */
        int hiltCents = 50;
        int pencilCents = 7;
        int howManyPencils = hiltCents / pencilCents;
        System.out.println("How many pencils can she get: " + howManyPencils);  
        /*    
        28. Mrs. Hilt saw 33 butterflies. Some of the butterflies were red and others
        were orange. If 20 of the butterflies were orange, how many of them
        were red?
        */
        int totalButterflies = 33;
        int orangeButterflies = 20;
        int redButterflies = totalButterflies - orangeButterflies;
        System.out.println("How many red butterflies are there: " + redButterflies);
        /*    
        29. Kate gave the clerk $1.00. Her candy cost 54 cents. How much change
        should Kate get back?
        */
        double kateMoney = 1.00;
        double kateCandyCost = 0.54;
        double kateChange = kateMoney - kateCandyCost;
        System.out.println("How much change does Kate get back: " + kateChange + " dollars");
        /*
        30. Mark has 13 trees in his backyard. If he plants 12 more, how many trees
        will he have?
        */
        int orginalTrees = 13;
        int newTrees = 12;
        int totalTrees = orginalTrees + newTrees;
        System.out.println("How many trees does mark have: " + totalTrees);
        		
        /*    
        31. Joy will see her grandma in two days. How many hours until she sees
        her?
        */
        int numberOfDays = 2;
        int hoursInADay = 24;
        int totalHours = numberOfDays * hoursInADay;
        System.out.println("How many hours until: " + totalHours);
        /*
        32. Kim has 4 cousins. She wants to give each one 5 pieces of gum. How
        much gum will she need?
        */
        int kimCousins = 4;
        int gumPieces = 5;
        int gumNeeds = kimCousins * gumPieces;
        System.out.println("How much  gum does she need: " + gumNeeds);
        /*
        33. Dan has $3.00. He bought a candy bar for $1.00. How much money is
        left?
        */
        double danMoney = 3.00;
        double danCandy = 1.00;
        double danMoneyLeft = danMoney - danCandy;
        System.out.println("How much money does dan have left: " + danMoneyLeft);
        /*
        34. 5 boats are in the lake. Each boat has 3 people. How many people are
        on boats in the lake?
        */
        int boatsOnLake = 5;
        int peopleInBoat = 3;
        int totalBoatPeople = boatsOnLake * peopleInBoat;
        System.out.println("How many people on the lake: " + totalBoatPeople);	
        /*
        35. Ellen had 380 legos, but she lost 57 of them. How many legos does she
        have now?
        */
        int totalLegos = 380;
        int lostLegos = 57;		
        int legosLeft = totalLegos - lostLegos;
        System.out.println("How many legos are left: " + legosLeft);
        /*
        36. Arthur baked 35 muffins. How many more muffins does Arthur have to
        bake to have 83 muffins?
        */
        int arthurMuffins = 35;
        int arthurTotalMuffins = 83;
        int arthurNeededMuffins = arthurTotalMuffins - arthurMuffins;
        System.out.println("How many muffins does he need: " + arthurNeededMuffins);
        		
        /*
        37. Willy has 1400 crayons. Lucy has 290 crayons. How many more
        crayons does Willy have then Lucy?
        */
        int willyCrayons = 1400;
        int lucyCrayons = 290;
        int moreCrayons = willyCrayons - lucyCrayons;
        System.out.println("How many more crayons does Willy have: " + moreCrayons);
        		
        /*
        38. There are 10 stickers on a page. If you have 22 pages of stickers, how
        many stickers do you have?
        */
        int stickersOnPage = 10;
        int numberOfPages = 22;
        int stickersTotal = stickersOnPage * numberOfPages;
        System.out.println("How many sticker do you have: " + stickersTotal);
        		
        /*
        39. There are 96 cupcakes for 8 children to share. How much will each
        person get if they share the cupcakes equally?
        */
        int totalChildrenCupcakes = 96;
        int numberOfChildren = 8;
        int cupcakesPerPerson = totalChildrenCupcakes / numberOfChildren;
        System.out.println("How many cupcakes does each person get: " + cupcakesPerPerson);
		
   
        /*
        40. She made 47 gingerbread cookies which she will distribute equally in
        tiny glass jars. If each jar is to contain six cookies each, how many
        cookies will not be placed in a jar?
        */
        int numberGingerbread = 47;
        int numberPerJar = 6;
        int remainingCookies = numberGingerbread % numberPerJar;
        System.out.println("How many cookies are left: " + remainingCookies);
		
        		
        /*
        41. She also prepared 59 croissants which she plans to give to her 8
        neighbors. If each neighbor received and equal number of croissants,
        how many will be left with Marian?
        */
        int numberCroissants = 59;
        int numberPerNeighbor = 8;
        int remainingCroissants = numberCroissants % numberPerNeighbor;
        System.out.println("How many croissants are left: " + remainingCroissants);
        /*
        42. Marian also baked oatmeal cookies for her classmates. If she can
        place 12 cookies on a tray at a time, how many trays will she need to
        prepare 276 oatmeal cookies at a time?
        */
        int totalOatmealCookies = 276;
        int cookiesPerTray = 12;
        int numberOfTrays = totalOatmealCookies / cookiesPerTray;
        System.out.println("How many trays do you need: " + numberOfTrays);
        /*
        43. Marian’s friends were coming over that afternoon so she made 480
        bite-sized pretzels. If one serving is equal to 12 pretzels, how many
        servings of bite-sized pretzels was Marian able to prepare?
        */
        int totalPretzels = 480;
        int servingPretzels = 12;
        int totalServings = totalPretzels / servingPretzels;
        System.out.println("How many servings are there: " + totalServings);
        		
        /*
        44. Lastly, she baked 53 lemon cupcakes for the children living in the city
        orphanage. If two lemon cupcakes were left at home, how many
        boxes with 3 lemon cupcakes each were given away?
        */
        int totalLemonCupcakes = 53;
        int lemonCupcakesHome = 2;
        int cupcakesPerBox = 3;
        int boxesGiven = (totalLemonCupcakes - lemonCupcakesHome) / cupcakesPerBox;
        System.out.println("How many boxes were given out: " + boxesGiven);		
        /*
        45. Susie's mom prepared 74 carrot sticks for breakfast. If the carrots
        were served equally to 12 people, how many carrot sticks were left
        uneaten?
        */
        int totalCarrots = 74;
        int numberOfPeople = 12;
        int remainingCarrots = totalCarrots % numberOfPeople;
        System.out.println("How many carrots were left: " + remainingCarrots);
        		
        /*
        46. Susie and her sister gathered all 98 of their teddy bears and placed
        them on the shelves in their bedroom. If every shelf can carry a
        maximum of 7 teddy bears, how many shelves will be filled?
        */
        int totalTeddyBears = 98;
        int bearsPerShelf = 7;
        int numberOfShelfs = totalTeddyBears / bearsPerShelf;
        System.out.println("How many shelfs are filled: " + numberOfShelfs);
        /*
        47. Susie’s mother collected all family pictures and wanted to place all of
        them in an album. If an album can contain 20 pictures, how many
        albums will she need if there are 480 pictures?
        */
        int picturesPerAlbum = 20;
        int numberOfPictures = 480;
        int totalAlbums = numberOfPictures / picturesPerAlbum;
        System.out.println("How many albums do you need: " + totalAlbums);
        		
        /*
        48. Joe, Susie’s brother, collected all 94 trading cards scattered in his
        room and placed them in boxes. If a full box can hold a maximum of 8
        cards, how many boxes were filled and how many cards are there in
        the unfilled box?
        */
        int totalCards = 94;
        int cardsPerBox = 8;
        int boxesFilled = totalCards / cardsPerBox;
        int cardsLeft = totalCards % cardsPerBox;
        System.out.println("Number of boxes filled: " + boxesFilled + " and cards left: " + cardsLeft);
        /*
        49. Susie’s father repaired the bookshelves in the reading room. If he has
        210 books to be distributed equally on the 10 shelves he repaired,
        how many books will each shelf contain?
        */
        int fatherBooks = 210;
        int fathersShelfs = 10;
        int booksPerShelf = fatherBooks / fathersShelfs;
        System.out.println("How many books per shelf:" + booksPerShelf);
        		
        /*
        50. Cristina baked 17 croissants. If she planned to serve this equally to
        her seven guests, how many will each have?
        */
        int cristinaCroissants = 17;
        int numberOfGuests = 7;
        int eachPerson = cristinaCroissants / numberOfGuests;
        System.out.println("How many croissants does each person get: "
  
        		+ "" + eachPerson);   
	}

}
