package com.techelevator;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CarTest {
		private Car newCar;

		@Before
		public void setup() {
			newCar = new Car("Car", 300, 20.00, true);
		}


		@Test
		public void initialization_values_are_stored_correctly() {
		    String vehicle = newCar.getVehicle();
		    int distanceTraveled = newCar.getDistanceTraveled();
		    double toll = newCar.getToll();
		    boolean pullingTrailer = newCar.getPullingTrailer();
		    Assert.assertEquals("Car", vehicle);
		    Assert.assertEquals(newCar.getDistanceTraveled(), distanceTraveled);
		    Assert.assertEquals(newCar.getToll(), toll, 0.01);
		    Assert.assertEquals(true, pullingTrailer);

		}@Test
	    public void check_toll_variables() {
	    	double toll= newCar.getDistanceTraveled() * 0.020;
	    	Assert.assertEquals(newCar.getDistanceTraveled() *  0.020, toll ,0.01);
		
	}
}