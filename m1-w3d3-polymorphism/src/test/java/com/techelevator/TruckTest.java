package com.techelevator;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TruckTest {
		private Truck newTruck;

		@Before
		public void setup() {
			newTruck = new Truck("Truck",200, 20.00, 8);
		}


		@Test
		public void initialization_values_are_stored_correctly() {
		    String vehicle = newTruck.getVehicle();
		    int distanceTraveled = newTruck.getDistanceTraveled();
		    double toll = newTruck.getToll();
		    int numberOfAxles = newTruck.getNumberOfAxles();
		    Assert.assertEquals("Truck", vehicle);
		    Assert.assertEquals(newTruck.getDistanceTraveled(), distanceTraveled);
		    Assert.assertEquals(newTruck.getToll(), toll, 0.01);
		    Assert.assertEquals(8, numberOfAxles);

		}
		@Test
	    public void calculate_number_of_axles() {
			newTruck.getNumberOfAxles();
	    	int numberOfAxles = (int) newTruck.getNumberOfAxles();
	    	Assert.assertEquals(newTruck.getNumberOfAxles(),numberOfAxles, 0.01);
		
	}
}