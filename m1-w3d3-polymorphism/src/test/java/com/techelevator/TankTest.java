package com.techelevator;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TankTest {
	private Tank newTank;

	@Before
	public void setup() {
		newTank = new Tank("Tank",200, 20.00);
	}


	@Test
	public void initialization_values_are_stored_correctly() {
	    String vehicle = newTank.getVehicle();
	    int distanceTraveled = newTank.getDistanceTraveled();
	    double toll = newTank.getToll();
	    Assert.assertEquals("Tank", vehicle);
	    Assert.assertEquals(newTank.getDistanceTraveled(), distanceTraveled);
	    Assert.assertEquals(0, toll, 0.01);

	}
}