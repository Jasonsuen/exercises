package com.techelevator;


import java.util.List;
import java.util.Scanner;

public class Postage implements DeliveryDriver{

public static void main(String[] args) {
// char weightUnit;
// double shippingDistance;
Scanner reader = new Scanner(System.in);

    System.out.println("Please enter the weight of the package?");
    int packageWeight = reader.nextInt();

    System.out.println("(P)ounds or (O)unces?");
    String unit = reader.next();
    
    if(unit.equals("P")) {
        packageWeight *= 16;
    } 
    
    System.out.println("What distance will it be traveling to?");
    int distance = reader.nextInt();
    
    
    DeliveryDriver[] carrier = new DeliveryDriver[] {new PostalService(), new FexEd(), new Spu() };
            
        for(DeliveryDriver delivery : carrier) {
            List<DeliveryMethod> deliveryList = delivery.calculateRate(distance, packageWeight);
            
            for(DeliveryMethod newDeliveryOptionsList : deliveryList) {
                System.out.println(newDeliveryOptionsList.getCarrierName() + " " + newDeliveryOptionsList.getPrice());
            }
        }


}// end main

@Override
public List<DeliveryMethod> calculateRate(double distance, double weight) {
    return null;
}
}