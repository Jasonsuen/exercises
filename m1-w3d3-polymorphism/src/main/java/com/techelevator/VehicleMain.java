package com.techelevator;

public class VehicleMain {
	public String vehicle;
	public int distanceTraveled;
	public double toll;

	
	
		public VehicleMain (String vehicle, int distanceTraveled, double toll){
			this.vehicle = vehicle;
			this.distanceTraveled = distanceTraveled;
			this.toll = toll;
		
		}
		public static void main(String[] args) {
		VehicleToll[] vehicleToll = new VehicleToll[] { new Car("Car", 300, 20.00, true), new Truck("Truck", 200, 10.00, 8 ), new Tank("Tank",150, 0) };
			System.out.println("Vehicle" + "\t\t\t" +  "Distance Traveled" + "\t\t" + "Toll $");
			System.out.println("----------------------------------------------------------------");
		for(VehicleToll vehicleTolls  : vehicleToll) {
			String vehicle = vehicleTolls.getVehicle();
			int distanceTraveled = vehicleTolls.getDistanceTraveled();
			double toll= vehicleTolls.getToll();
			System.out.println(vehicle + "\t\t\t" +distanceTraveled + "\t\t\t\t" + toll);
		}	
		}
	}

