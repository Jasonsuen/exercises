package com.techelevator;

public interface VehicleToll {
	
	public String getVehicle();
	public int getDistanceTraveled();
	public double getToll();

}
