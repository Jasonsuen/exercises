package com.techelevator;

public class FexEd implements DeliveryDriver {
	
	@Override
	public List<DeliveryMethod> calculateRate(double distance, double weight) {

	    List<DeliveryMethod> newList = new ArrayList<DeliveryMethod>();
	    
	    double rate = 20.00;
	    
	    if(weight > 48) {
	        rate = rate + 3.00;
	    } 

	    if(distance > 500) {
	        rate = rate + 5.00;
	    }
	    DeliveryMethod newMethod = new DeliveryMethod("FedEx", rate);
	    
	    newList.add(newMethod);
	    
	    return newList;
	}
}
