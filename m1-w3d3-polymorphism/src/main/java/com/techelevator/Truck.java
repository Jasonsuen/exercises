package com.techelevator;

public class Truck implements VehicleToll {
	public String vehicle;
	public int distanceTraveled;
	public double toll;
	public int numberOfAxles;
	
	public Truck (String vehicle, int distanceTraveled, double toll, int numberOfAxles) {
		this.vehicle = vehicle;
		this.distanceTraveled = (int) (Math.random() * 240 + 10);
		this.toll = toll;
		this.numberOfAxles = numberOfAxles;
	
	}
	@Override
	public String getVehicle() {
		return vehicle;
	}

	@Override
	public int getDistanceTraveled() {
		return distanceTraveled;
	}
	public int getNumberOfAxles() {
		return numberOfAxles;
	}
	@Override
	public double getToll() {
		if(getNumberOfAxles() <= 4){
			toll = 0.040 * distanceTraveled;
		}else if(getNumberOfAxles() <= 6){
			toll = 0.045 * distanceTraveled;
		}else if (getNumberOfAxles() <= 8) {
			toll = 0.048 * distanceTraveled;
		}
		return toll;
	}

}

