package com.techelevator;

public class Tank implements VehicleToll {
		public String vehicle;
		public int distanceTraveled;
		public double toll;
		
		
		public Tank (String vehicle, int distanceTraveled, double toll) {
			this.vehicle = vehicle;
			this.distanceTraveled = (int) (Math.random() * 240 + 10);
			this.toll = toll;		
		}
		@Override
		public String getVehicle() {
			return vehicle;
		}

		@Override
		public int getDistanceTraveled() {
			return distanceTraveled;
		}

		@Override
		public double getToll() {
			double toll = 0;
			return toll;
		}

	}

