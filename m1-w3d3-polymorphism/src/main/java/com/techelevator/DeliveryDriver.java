package com.techelevator;

public interface DeliveryDriver {
	
	
	public int getDistance( );
	public double getWeight( );
	public double getRate( );
}
