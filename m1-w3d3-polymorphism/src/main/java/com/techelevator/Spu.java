package com.techelevator;


	import java.util.ArrayList;
	import java.util.List;

	public class Spu implements DeliveryDriver{

	@Override
	public List<DeliveryMethod> calculateRate(double distance, double weight) {
	    
	    
	    List<DeliveryMethod> newList = new ArrayList<DeliveryMethod>();
	    
	    double fourDayRate = weight * 0.0050;
	    double twoDayRate = weight * 0.050;
	    double nextDayRate = weight * 0.075;
	            
	    DeliveryMethod fourDayDelivery = new DeliveryMethod("SPU (4-day ground) ", fourDayRate);
	    newList.add(fourDayDelivery);
	    
	    DeliveryMethod twoDayDelivery = new DeliveryMethod("SPU (2-dy business) ", twoDayRate);
	    newList.add(twoDayDelivery);
	    
	    DeliveryMethod nextDayDelivery = new DeliveryMethod("SPU (next-day) ", nextDayRate);
	    newList.add(nextDayDelivery);
	    
	    return newList;
	    
	}// end override
	}// end public class
