package com.techelevator;

public class Car implements VehicleToll {
	public String vehicle;
	public int distanceTraveled;
	public double toll;
	public boolean pullingTrailer;
	
	public Car (String vehicle, int distanceTraveled, double toll, boolean pullingTrailer) {
		this.vehicle = vehicle;
		this.distanceTraveled = (int) (Math.random() * 240 + 10);;
		this.toll = toll;
		this.pullingTrailer = pullingTrailer;
	
	}
	@Override
	public String getVehicle() {
		return vehicle;
	}

	@Override
	public int getDistanceTraveled() {
		return distanceTraveled;
	}

	@Override
	public double getToll() {
		if(pullingTrailer){
			double toll =  (getDistanceTraveled() * 0.020) + 1.00;
			return toll;
		} else 
			toll = getDistanceTraveled() * 0.020; 
		return toll;
	}
	public boolean getPullingTrailer() {
		return pullingTrailer  = true;
	}

}
