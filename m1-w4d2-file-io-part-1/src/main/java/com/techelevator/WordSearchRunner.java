package com.techelevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class WordSearchRunner {
		public WordSearchRunner(String fileName) {
		BufferedReader reader = null;
		Scanner scanner = null;

		    try {
		        ClassLoader classLoader = getClass().getClassLoader();
		        File readFile = new File(classLoader.getResource(fileName).getFile());
		        FileReader fileReader = new FileReader(readFile);

		        reader = new BufferedReader(fileReader);
		        scanner = new Scanner(System.in);


		        String lines;

		        System.out.println("What term would you like to search for?");
		        String searchValue = scanner.next();

		        System.out.println("Would you like this search to be case sensitive? (Y)  (N)");
		        String caseSensitive = scanner.next();

		        System.out.println("The following is a list of lines that contain: " + searchValue);
		        System.out.println("");


		        int totalLines = 0;
		        if(caseSensitive.equals("Y")){
		            while((lines = reader.readLine()) != null) {
		            totalLines++;           

		            if(lines.contains(searchValue)){
		                System.out.println(totalLines+") \t" + lines);
		            } 
		        }   
		        
		        } else {
		            
		            String valueUppercase = searchValue.toUpperCase();
		            String valueLowercase = searchValue.toLowerCase();
		            
		            while((lines = reader.readLine()) != null) {
		            totalLines++;           

		            if( lines.contains(searchValue) || (lines.contains(valueUppercase) || (lines.contains(valueLowercase)))){
		                System.out.println(totalLines+") \t" + lines);
		            }
		            }
		        }

		    } catch (FileNotFoundException e) {
		        System.err.println("File was not found!");
		        e.printStackTrace();
		    } catch (IOException e) {
		        e.printStackTrace();
		    } finally {
		        if(reader != null) {
		            try {
		                reader.close();
		            } catch (IOException e) {
		                e.printStackTrace();
		            }
		        }
		    }
		}
		}
}
}