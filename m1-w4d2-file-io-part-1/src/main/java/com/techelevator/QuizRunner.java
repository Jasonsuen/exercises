package com.techelevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class QuizRunner {
	public QuizRunner(String fileName) {
		BufferedReader reader = null; 
		Scanner scanner = null;
		
		try { 
			ClassLoader classLoader = getClass().getClassLoader();
			File quizFile = new File(classLoader.getResource(fileName).getFile());
			FileReader fileReader = new FileReader(quizFile);
			reader = new BufferedReader(fileReader);
			scanner = new Scanner(System.in); 
			
			
			int totalCorrect = 0;
			String line = reader.readLine();
			while(line != null) {
				int correctAnswer = askQuestion(line);
				boolean wasCorrect = getUserChoice(scanner,correctAnswer);
				if(wasCorrect){
					totalCorrect++;
				}
				line = reader.readLine();
			}
		
			System.out.println("You got " + totalCorrect + " answer(s) right!");
			
		} catch (FileNotFoundException notFound) {
			System.err.println("File was not found");
			notFound.printStackTrace();
		} catch (IOException inputOuput) {
			System.err.println("Input does not match");
			inputOuput.printStackTrace();
		} finally {
			if(reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(scanner != null);
			scanner.close();
			}
		}
	/**
	 * Ask the user a single question and returns the correct answer index
	 * @param question A pipe delimited string containing the question and the possible choices. 
	 * The correct choice is marked by an *.
	 * @return the correct answer index
	 */
	private int askQuestion(String question) { 
		String[] parts = question.split("\\|");
		System.out.println(parts[0]);
		int correctAnswer = 0;
		
		for(int i = 1; i < parts.length; i++) {
			String answer = parts[i];
			
			if(answer.endsWith("*")){
				correctAnswer = i;
				answer = answer.replace("*", "");
			
			}
			System.out.println((i + ".") + answer);
			
		}
		return correctAnswer;
}
	/**
	 *  Prompt user for their choice and return whether they are correct
	 * @param correctAnswer index of the correct answer
	 * @return True if the user selected the correct answer. Otherwise false.
	 */
	
	private boolean getUserChoice(Scanner scanner,int correctAnswer) {
		int userChoice = scanner.nextInt();
		if(userChoice == correctAnswer) {
			System.out.println("You got it! You rock!");
			return true;
		} else { 
			System.out.println("Wrong Dummy!");
			return false;
		}
	}
}