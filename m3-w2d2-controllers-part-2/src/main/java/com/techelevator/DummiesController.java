package com.techelevator;



import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.Review;
import com.techelevator.model.ReviewDao;



@Controller 
public class DummiesController {
	
	@Autowired
	ReviewDao reviewDAO;
	
	@RequestMapping(path="/homepage", method=RequestMethod.GET)
	public String displayHomepage(HttpServletRequest request) {
		request.setAttribute("homepage", reviewDAO.getAllReviews());
		return "homepage";
	}

	@RequestMapping(path="/dummiesInput", method=RequestMethod.GET)
	public String displayDummiesInput() {
		return "dummiesInput";
	}
	
	@RequestMapping(path="/dummiesInput", method=RequestMethod.POST)
	public String processSpaceForumInput(@RequestParam String userName,
										@RequestParam int rating,
										@RequestParam String title,
										@RequestParam String text
										)  { 
		
		Review review = new Review();
		review.setUsername(userName);
		review.setRating(rating);
		review.setTitle(title);
		review.setText(text);
		review.setDateSubmitted(LocalDateTime.now());
		reviewDAO.save(review);
		
		return "redirect:/homepage";
	}
	
}