<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Squirrel Parties for Dummies Homepage</title>
		<link rel="stylesheet" href="site.css"></link>
	</head>
	<body>
		<div id="main_content">
			<h1>Who doesn't like squirrel parties?</h1>
			<p></p>
			<ol>
				<li>Grab your mouse (or other pointing device of choice)</li>
				<li>Assure yourself that you deserve to <strong>be more awesome</strong></li>
				<li><strong>Click the green button</strong></li>
				<li>Provide your name and email address and we'll let you know when it's time to <strong>unveil the awesomeness</strong></li>
			</ol>
			
			<a href="notificationSignupInput"><img alt="I Want Awesome Button" src="img/button.png"></a>
		</div>
	</body>
</html>