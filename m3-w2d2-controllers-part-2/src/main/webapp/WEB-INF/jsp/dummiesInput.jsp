<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Squirrel Parties for Dummies Homepage</title>
		<link rel="stylesheet" href="site.css"></link>
	</head>
	<body>
		<body>
		<div id="main_content">
			<h1>Submit Your Review For This Awesome Book</h1>
			<form action="dummiesInput" method="POST">
				<div class="formGroup">
					<label for="userName">Username </label>
					<input type="text" name="userName" id="userName" />
				</div>
				<div class="formGroup">
					<label for="rating">Rating </label>
					<input type="text" name="rating" id="rating" />
				</div>	
				<div class="formGroup">		
					<label for="title">Review Title </label>
					<input type="text" name="title" id="title" />
				</div>	
				<div class="formGroup">		
					<label for="text">Review Text </label>
					<input type="text" name="text" id="text" />
				</div>
				<div class="formGroup">
					<input type="submit" value="Submit Review" />
				</div>		
			</form>
		</div>
	</body>
</html>