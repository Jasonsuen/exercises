<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<title>Squirrel Parties for Dummies Homepage</title>
<link rel="stylesheet" href="site.css"></link>
</head>
<img src="image/forDummies.png"/>	

<body>
	<div id="main_content">
		<h1>Who doesn't like squirrel parties?</h1>

		<c:forEach var="review" items="${homepage}">
			<div>
				<div ><b>${review.title}</b> (${review.username})</div>
				<div >${review.dateSubmitted}</div>
				<c:forEach begin="1" end="${review.rating}" var="loop">
					<img src="image/star.png"/>				
					</c:forEach> 
				<div >${review.text}</div>
				<p> </p>
			</div>
		</c:forEach>
		<a href="dummiesInput">Submit Your Review</a>
	</div>
</body>
</html>