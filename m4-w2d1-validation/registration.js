$(document).ready(function () {

    $("#applicationForm").validate({

        debug: true,
        rules: {
			
            email: {
                required: true,
				email: true
            },
            firstName: {
                required: true
			},
            lastName: {
                required: true
			},
            password: {
                required: true,
				minlength: 8,
				strongpassword: true
			},
			confirm: {
	                equalTo: "#password"
	            },
			country: {
				required: true
			},
			homePhone: {
				phoneUS: true
			},
			officePhone: {
				phoneUS: true
			},
			cellPhone: {
				phoneUS: true
			},
		},
	 	messages: {
			email: {
			    required: "You must provide a email",
				email: "Must be a valid email address"
			    },
			password: {
			    required: "You must provide a password"
			    },
			 },
		errorClass: "error",
		        validClass: "valid"
    });
  });
  
  $.validator.addMethod("strongpassword", function (value, index) {
      return value.match(/[A-Z]/) && value.match(/[a-z]/) && value.match(/\d/);
  }, "Please enter a strong password (one capital, one lower case, and one number)");
