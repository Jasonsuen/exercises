$(document).ready(function () {

    $("#applicationForm").validate({

        debug: true,
        rules: {
			
            username: {
                required: true,
				email: true,
            },
			
            password: {
                required: true,
			},
	    messages: {
	        username: {
	            required: "You must provide a username",
				email: "Must be a valid email address"
	        },
	        password: {
	            required: "You must provide a password"
	            }
	        },   
		errorClass: "error",
        validClass: "valid"
		}
    });
  });
