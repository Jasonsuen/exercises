$(document).ready(function () {
    
    $("#applicationForm").validate({

        debug: true,
        rules: {
			
            birthday: {
                required: true,
				date: true
            },
			
            ssn: {
                required: true,
				ssn: true
				
			},
			address2: {
				empty: {
						depends: function (element) {
						return $("#address1").is(":blank");
					}
				}
			},
			
			city: {
				required: true
			},
			state: {
				required: true
			},
			zip: {
				required: true,
				zipcode: true
			},
			gender: {
				required: true
			},
		},
			
	    messages: {
	        bithday: {
	            required: "You must provide a birthdate",
				date: "Must be a valid date (mm/dd/yyyy)"
	        },
	        ssn: {
	            required: "You must provide a ssn"
	        },
		    city: {
		      required: "You must provide a city"
		    },
		    state: {
		      required: "You must provide a state"
		    },
		    zip: {
		      required: "You must provide a zip code"
		    },
		    gender: {
		      required: "You must provide a gender"
		    },
			},   
		errorClass: "error",
        validClass: "valid"
    });
  });
  
  jQuery.validator.addMethod("zipcode", function(value, element) {
    return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
  }, "Please provide a valid zipcode.");
 
  jQuery.validator.addMethod('empty', function(value, element) {
  return (value === '');
  }, "This field must remain empty if the first address line is empty");

  jQuery.validator.addMethod("ssn", function(value, element) {
	  return this.optional(element) || /^(\d{3})-?\d{2}-?\d{4}$/i.test(value);
  }, "Invalid ssn");
