$(document).ready(function () {
		$("#allNewsletters").click(function(){
			var checked = $(this).prop("checked");
			$('.subcheck').prop('checked', checked);
			});
		 
		$("#applicationForm").validate({

		        debug: true,
		        rules: {

		            email: {
		                required: true,
						email: true
		            },

		            password: {
		                required: true,
		                minlength: 8,
		                strongpassword: true
					},
		            confirm: {
		                equalTo: "#password"
		            },
		            company: {
		                required: true
		            },
		            website: {
		                required: true,
						url: true
		            },
		            industry: {
		                required: true
		            },
		           	position: {
		                required: true
		            },
		            numberOfEmployees: {
		                required: true
		            },


			        messages: {
				        username: {
				            required: "You must provide a username",
							email: "Must be a valid email address"
				        },
				        password: {
				            required: "You must provide a password"
				        }
				    }, 
					errorClass: "error",
		       	 	validClass: "valid"
					}
		    });
  });
  
  $.validator.addMethod("strongpassword", function (value, index) {
      return value.match(/[A-Z]/) && value.match(/[a-z]/) && value.match(/\d/);  //check for one capital letter, one lower case letter, one num
  }, "Please enter a strong password (one capital, one lower case, and one number)");
  
