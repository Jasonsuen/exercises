package com.techelevator;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;


@Controller
@SessionAttributes("favorites")
public class FavoriteController {

	@RequestMapping(path={"/", "/page1"}, method=RequestMethod.GET)
	public String displayPage1() {
		return "page1";
	}
	
	@RequestMapping(path="/page1", method=RequestMethod.POST)
	public String processPage1Data(@RequestParam String color,  ModelMap model){
		
		Favorites favorites = new Favorites();
		favorites.setColor(color);
		
		model.addAttribute("favorites", favorites);
		
		return "redirect:/page2";
	}
	
	@RequestMapping(path="/page2", method=RequestMethod.GET)
	public String displayPage2() {
		return "page2";
	}
	
	@RequestMapping(path="/page2", method=RequestMethod.POST)
	public String processPage2Data(@RequestParam String food,  ModelMap model){
		
		Favorites favorites = (Favorites)model.get("favorites");
		favorites.setFood(food);
		
		
		return "redirect:/page3";
	}
	
	@RequestMapping(path= "/page3", method=RequestMethod.GET)
	public String displayPage3() {
		return "page3";
	}
	
	@RequestMapping(path="/page3", method=RequestMethod.POST)
	public String processPage3Data(@RequestParam String season,  ModelMap model){
		
		Favorites favorites = (Favorites)model.get("favorites");
		favorites.setSeason(season);
		
		return "redirect:/summary";
	}
	
	@RequestMapping(path= "/summary", method=RequestMethod.GET)
	public String displaySummary() {
		return "summary";
	}
	
}
