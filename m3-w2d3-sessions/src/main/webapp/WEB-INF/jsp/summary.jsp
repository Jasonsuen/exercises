<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Favorites</title>
	</head>
	<body>
		<h1>Favorite Things Exercise"</h1>
		<form method="GET" action="summary">
			<p> <b>Favorite Color :</b> ${favorites.color}</p>
			<p> <b>Favorite Food :</b> ${favorites.food}</p>
			<p> <b>Favorite Season :</b> ${favorites.season}</p>
		</form>
		
	</body>
</html>